<?php
/**
 *
 */
?>

<?php do_action( 'listify_map_before' ); ?>

<div class="ljm_repertoires-map-wrapper">
	<?php do_action( 'listify_map_above' ); ?>

	<div class="ljm_repertoires-map">
		<div id="ljm_repertoires-map-canvas"></div>
	</div>

	<?php do_action( 'listify_map_below' ); ?>
</div>

<?php do_action( 'listify_map_after' ); ?>