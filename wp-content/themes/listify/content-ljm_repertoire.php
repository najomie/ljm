<?php
/**
 * The template for displaying job listings (in a loop).
 *
 * @package Listify
 */
?>

<li id="ljm_repertoire-<?php the_ID(); ?>" <?php ljm_repertoire_class(); ?> <?php echo apply_filters(
'listify_ljm_repertoire_data', '', false ); ?>>

	<div class="content-box">

		<a href="<?php the_permalink(); ?>" class="ljm_repertoire-clickbox"></a>

		<header <?php echo apply_filters( 'listify_cover', 'ljm_repertoire-entry-header listing-cover' ); ?>>
            <?php do_action( 'listify_content_ljm_repertoire_header_before' ); ?>

			<div class="ljm_repertoire-entry-header-wrapper cover-wrapper">

				<div class="ljm_repertoire-entry-thumbnail">
					<div <?php echo apply_filters( 'listify_cover', 'list-cover' ); ?>></div>
				</div>
				<div class="ljm_repertoire-entry-meta">
					<?php do_action( 'listify_content_ljm_repertoire_meta' ); ?>
				</div>

			</div>

            <?php do_action( 'listify_content_ljm_repertoire_header_after' ); ?>
		</header><!-- .entry-header -->

		<footer class="ljm_repertoire-entry-footer">

			<?php do_action( 'listify_content_ljm_repertoire_footer' ); ?>

		</footer><!-- .entry-footer -->

	</div>
</li><!-- #post-## -->
