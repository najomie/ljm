<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Listify
 */

get_header(); ?>

   <header <?php echo apply_filters( 'listify_cover', 'entry-header entry-cover' ); ?>>
      <div class="cover-wrapper">
         <h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
      </div>
   </header><!-- .entry-header -->

    <div id="primary" class="container">
        <div class="row content-area">

            <?php if ( 'left' == esc_attr( listify_theme_mod( 'content-sidebar-position' ) ) ) : ?>
                <?php get_sidebar(); ?>
            <?php endif; ?>

            <main id="main" class="site-main col-xs-12 <?php if ( 'none' != esc_attr( listify_theme_mod( 'content-sidebar-position' ) ) ) : ?>col-sm-7 col-md-8<?php endif; ?>" role="main">

                <?php while ( have_posts() ) : the_post(); ?>

                    <?php get_template_part( 'content' ); ?>

                    <?php comments_template(); ?>

                <?php endwhile; ?>

            </main>

            <?php if ( 'right' == esc_attr( listify_theme_mod( 'content-sidebar-position' ) ) ) : ?>
                <?php get_sidebar(); ?>
            <?php endif; ?>

        </div>
    </div>

<?php get_footer(); ?>
