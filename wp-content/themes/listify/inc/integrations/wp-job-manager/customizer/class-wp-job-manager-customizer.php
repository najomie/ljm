<?php

class Listify_WP_Job_Manager_Customizer {

	private $terms = array();

    public function __construct() {
        $panels = array( 'listings' );

        foreach ( $panels as $panel ) {
            require_once( dirname( __FILE__ ) . '/panels/class-customizer-panel-' . $panel . '.php' );
        }

        add_filter( 'listify_customizer_panels', array( $this, 'panels' ) );
        add_action( 'customize_register', array( $this, 'set_panels' ), 12 );
        add_filter( 'listify_theme_mod_defaults', array( $this, 'theme_mod_defaults' ) );
    }

    public function panels( $panels ) {
        $panels[] = 'listings';

        return $panels;
    }

    public function set_panels() {
        listify_customizer()->panels->listings = new Listify_Customizer_Panel_Listings();
    }

    /**
     * Create a blank default so we knwo what to retrieve
     */
    public function theme_mod_defaults( $mods ) {
        $mods[ 'label-singular' ] = __( 'Listing', 'listify' );
        $mods[ 'label-plural' ] = __( 'Listings', 'listify' );
        $mods[ 'region-bias' ] = 'US';
        $mods[ 'custom-submission' ] = true;
        $mods[ 'social-association' ] = 'user';
        $mods[ 'categories-only' ] = true;
        $mods[ 'gallery-comments' ] = true;

        $mods[ 'listing-archive-output' ] = 'map-results';
        $mods[ 'listing-archive-map-position' ] = 'side';
        $mods[ 'listing-archive-display-style' ] = 'grid';

        $mods[ 'listing-single-sidebar-position' ] = 'right';

        $mods[ 'map-appearance-scheme' ] = 'Default';

        $mods[ 'map-behavior-trigger' ] = 'mouseover';
        $mods[ 'map-behavior-clusters' ] = 1;
        $mods[ 'map-behavior-grid-size' ] = 60;
        $mods[ 'map-behavior-autofit' ] = 1;
        $mods[ 'map-behavior-center' ] = '';
        $mods[ 'map-behavior-zoom' ] = 3;
        $mods[ 'map-behavior-max-zoom' ] = 17;
        $mods[ 'map-behavior-max-zoom-out' ] = 3;
        $mods[ 'map-behavior-search-min' ] = 0;
        $mods[ 'map-behavior-search-max' ] = 100;
        $mods[ 'map-behavior-search-default' ] = 50;
        $mods[ 'map-behavior-scrollwheel' ] = 'on';

		$taxonomies = array( get_taxonomy( listify_get_top_level_taxonomy() ) );
        $tags = get_taxonomy( 'job_listing_tag' );

        if ( $tags ) {
            $taxonomies[] = $tags;
        }

        foreach ( $taxonomies as $key => $taxonomy ) {
            $controls = array();

			if ( ! isset( $this->terms[ $taxonomy->name ] ) ) {
				$this->terms[ $taxonomy->name ] = listify_get_terms( $taxonomy->name );
			}

            foreach ( $this->terms[ $taxonomy->name ] as $term ) {
                $mods[ 'listings-' . $taxonomy->name . '-' . $term->slug . '-icon' ] = str_replace( 'ion-', '', get_theme_mod( 'marker-icon-' . $term->term_id ) );
            }
        }

		$taxonomies = array( get_taxonomy( listify_get_top_level_taxonomy() ) );

        foreach ( $taxonomies as $key => $taxonomy ) {
			if ( ! isset( $this->terms[ $taxonomy->name ] ) ) {
				$this->terms[ $taxonomy->name ] = listify_get_terms( $taxonomy->name );
			}

            foreach ( $this->terms[ $taxonomy->name ] as $term ) {
                $mods[ 'marker-color-' . $term->term_id ] = '#555555';
            }
        }

        return $mods;
    }

}
