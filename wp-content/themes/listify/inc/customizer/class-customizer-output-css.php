<?php

class Listify_Customizer_Output {

    public function __construct() {
        add_action( 'wp', array( $this, 'load_output' ) );
        add_action( 'wp', array( $this, 'output' ) );
    }

    public function load_output() {
        $output = array(
            'class-customizer-css-colors.php',
            'class-customizer-css-nav.php',
            'class-customizer-css-buttons.php',
            'class-customizer-css-content.php',
            'class-customizer-css-typography.php',
            'class-customizer-css-marker-appearance.php',
            'class-customizer-css-as-seen-on.php'
        );

        foreach ( $output as $file ) {
            include_once( trailingslashit( dirname( __FILE__) ) . 'css/' . $file );
        }
    }

    public function output() {
        $this->colors = new Listify_Customizer_CSS_Colors();
        $this->nav = new Listify_Customizer_CSS_Nav();
        $this->buttons = new Listify_Customizer_CSS_Buttons();
        $this->content = new Listify_Customizer_CSS_Content();
        $this->typography = new Listify_Customizer_CSS_Typography();
        $this->markers = new Listify_Customizer_CSS_Marker_Appearance();
        $this->aso = new Listify_Customizer_CSS_As_Seen_On();
    }

    public function get_regex_theme_mods( $property ) {
        $mods = listify_get_theme_mod_defaults();
        $font_keys = array();

        foreach ( $mods as $key => $default ) {
            if ( preg_match( "/{$property}/i", $key ) ) {
                $font_keys[] = $key;
            }
        }

        return $font_keys;
    }

}
