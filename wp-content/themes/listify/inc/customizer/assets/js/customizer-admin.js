(function( window, $, wp ) {

  var listifyCustomizer = listifyCustomizer || {};

  /**
   * Control Group
   */

  listifyCustomizer.ControlGroup = function(options) {
    this.container = $( options.container );

    this.bindEvents();
  }

  listifyCustomizer.ControlGroup.prototype.bindEvents = function() {
    var self = this;

    this.container.find( 'input' ).on( 'change', function() {
      self.updateControls( $(this).data( 'controls' ) );
    });
  }

  listifyCustomizer.ControlGroup.prototype.updateControls = function( controls ) {
    var self = this;

    $.each( controls, function(control, defaultVal) {
      var $control = $( '#customize-control-' + control );
      var $input = $control.find( 'input, select, textarea' );

      wp.customize( control, function(setting) {
        // color picker
        if ( String(defaultVal).charAt(0) == '#' ) {
          var $el = $control.find( '.color-picker-hex' );

          $el.wpColorPicker( 'color', defaultVal );
        } else {

          if ( $input.is( 'select' ) ) {
            $input
              .val( defaultVal )
              .trigger('chosen:updated')
          } else if ( $input.is( ':radio' ) ) {
            $control.find( 'input[value=' + defaultVal + ']').prop( 'checked', true );
          } else {
            $input.val( defaultVal );
          }
        }

        // make dirty so its saved
        setting.set(defaultVal);

        // climb down the tree
        if ( $input.data( 'controls' ) ) {
          self.updateControls( $( 'input[name=_customize-radio-' + control + ']:checked' ).data( 'controls') );
        }
      });
    });
  }

  /**
   * Chosen choices for huge select boxes
   */

  listifyCustomizer.bigChoices = function(args) {
    this.options = args.options;
    this.choices = args.choices;

    this.cache = {}
    this.cache.rtl = $('body').hasClass('rtl');
    this.cache.options = {};

    self = this;

    $.each(self.options, function(index, key) {
      self.cache.options[key] = $('select', '#customize-control-' + key);
    });

    // Build
    this.buildChoices();

    // Insert
    this.insertChoices();
  }

  listifyCustomizer.bigChoices.prototype.buildChoices = function() {
    self = this;
    self.cache.choices = '';

    $.each(self.choices, function(index, choice) {
      var value = choice.k,
          label = choice.l,
          disabled = (!isNaN(parseFloat(+value)) && isFinite(value)) ? ' disabled="disabled"' : '';

      self.cache.choices += '<option value="' + value + '"' + disabled + '>' + label + '</option>';
    });
  },

  listifyCustomizer.bigChoices.prototype.insertChoices = function() {
    self = this;

    $.each(self.cache.options, function(key, element) {
      if (self.cache.rtl) {
        element.addClass('chosen-rtl');
      }

      wp.customize( key, function(setting) {
        var v = setting.get();

        element.chosen({
          placeholder_text_single: listifyCustomizerOptions.chosen.placeholder,
          no_results_text : listifyCustomizerOptions.chosen.no_results_fonts,
          search_contains : true,
          width : '100%'
        });

        $(element)
          .html(self.cache.choices)
          .val( v )
          .trigger('chosen:updated');
      } );
    });
  }

  /**
   * Start
   */

  $(document).on( 'ready', function() {
    // control group toggles
    $.each( listifyControlGroups, function(key, group) {
      new listifyCustomizer.ControlGroup({
        container: group.container
      });
    });

    // font choices
    listifyCustomizer.fonts = new listifyCustomizer.bigChoices({
      options: listifyCustomizerOptions.fonts.options,
      choices: listifyCustomizerOptions.fonts.choices
    });

    // icon choices
    listifyCustomizer.icons = new listifyCustomizer.bigChoices({
      options: listifyCustomizerOptions.icons.options,
      choices: listifyCustomizerOptions.icons.choices
    });
  });

})( this, jQuery, wp );
