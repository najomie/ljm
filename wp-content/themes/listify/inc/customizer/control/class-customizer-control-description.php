<?php
/**
 * Description Control
 *
 * @since Listify 1.3.0
 */
class Listify_Customize_Description_Control extends WP_Customize_Control {
	public $type = 'description';

	public function render_content() {
?>
    <p><?php echo wp_kses_post( $this->label ); ?></p>
<?php
	}
}
