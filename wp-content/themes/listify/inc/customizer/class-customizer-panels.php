<?php

class Listify_Customizer_Panels {

    /**
     * array of registered panels
     */
    public $panels = array();

    public function __construct() {
        $this->panels = array( 'general', 'style-kits', 'typography', 'colors', 'layout', 'footer', 'nav_menus' );

        add_action( 'customize_register', array( $this, 'load_panels' ) );
        add_action( 'customize_register', array( $this, 'set_panels' ), 12 );
        add_action( 'customize_register', array( $this, 'register_panels' ), 15 );
    }

    public function load_panels() {
        foreach ( $this->panels as $panel ) {
            require_once( dirname( __FILE__ ) . '/panels/class-customizer-panel-' . $panel . '.php' );
        }
    }

    public function set_panels() {
        $this->general = new Listify_Customizer_Panel_General();
        $this->style_kits = new Listify_Customizer_Panel_Style_Kits();
        $this->typography = new Listify_Customizer_Panel_Typography();
        $this->colors = new Listify_Customizer_Panel_Colors();
        $this->layout = new Listify_Customizer_Panel_Layout();
        $this->footer = new Listify_Customizer_Panel_Footer();
        $this->nav_menus = new Listify_Customizer_Panel_Nav_Menus();
    }

    public function get_panels() {
        return apply_filters( 'listify_customizer_panels', $this->panels );
    }

    public function register_panels( $wp_customize ) {
        foreach ( $this->get_panels() as $panel ) {
            $panel = str_replace( '-', '_', $panel );

            if ( $wp_customize->get_panel( $panel ) ) {
                continue;
            }

            /*
             * This shouldn't happen often, but for when a panel only has controls, and no sections
             *
             * Should find a better way to do this
             */
            if ( empty( $this->$panel->sections ) && isset( $this->$panel->controls ) ) {
                $wp_customize->add_section( $this->$panel->id, $this->$panel->args );
            } else {
                $wp_customize->add_panel( $this->$panel->id, $this->$panel->args );
            }
        }
    }

}
