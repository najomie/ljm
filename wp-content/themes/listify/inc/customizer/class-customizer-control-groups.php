<?php
/**
 * Control Groups
 *
 * Style kits, font packs, color schemes, etc.
 *
 * @since Listify 1.3.0
 */

class Listify_Customizer_Control_Groups {

    public function __construct() {
        $this->groups = array( 'style-kit', 'color-scheme', 'font-pack' );

        $this->load_groups();
        $this->set_groups();

        add_action( 'customize_controls_enqueue_scripts', array( $this, 'customizer_scripts' ), 12 );
    }

    public function load_groups() {
        foreach ( $this->groups as $file ) {
            include_once( trailingslashit( dirname( __FILE__) ) . 'control-group/class-customizer-control-group-' . $file . '.php' );
        }
    }

    public function set_groups() {
        $this->color_scheme = new Listify_Customizer_Control_Group_Color_Scheme();
        $this->font_pack = new Listify_Customizer_Control_Group_Font_Pack();
        $this->style_kit = new Listify_Customizer_Control_Group_Style_Kit();
    }

    public function customizer_scripts() {
        $groups = array();

        foreach ( $this->groups as $group ) {
            $groups[] = array(
                'container' => '#customize-control-' . $group
            );
        }

        wp_localize_script( 'listify-customizer-admin', 'listifyControlGroups', $groups );
    }

}
