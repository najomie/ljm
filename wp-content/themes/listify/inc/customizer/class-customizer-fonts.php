<?php
/**
 * General Google Font methods.
 *
 * Mostly from Make
 * 
 * @see https://github.com/thethemefoundry/make
 */
class Listify_Customizer_Fonts {

    public function get_standard_fonts() {
        return apply_filters( 'listify_get_standard_fonts', array(
            'serif' => array(
                'label' => __( 'Serif', 'listify' ),
                'stack' => 'Georgia,Times,"Times New Roman",serif'
            ),
            'sans-serif' => array(
                'label' => __( 'Sans Serif', 'listify' ),
                'stack' => '"Helvetica Neue",Helvetica,Arial,sans-serif'
            ),
            'monospace' => array(
                'label' => __( 'Monospaced', 'listify' ),
                'stack' => 'Monaco,"Lucida Sans Typewriter","Lucida Typewriter","Courier New",Courier,monospace'
            )
        ) );
    }

    public function get_google_font_uri() {
        $fonts = array();
        $font_keys = listify_customizer()->output->get_regex_theme_mods( 'font-family' );

        foreach ( $font_keys as $key ) {
            $fonts[] = listify_theme_mod( $key );
        }

        // De-dupe the fonts
        $fonts         = array_unique( $fonts );
        $allowed_fonts = listify_get_google_fonts();
        $family        = array();

        // Validate each font and convert to URL format
        foreach ( $fonts as $font ) {
            $font = trim( $font );

            // Verify that the font exists
            if ( array_key_exists( $font, $allowed_fonts ) ) {
                // Build the family name and variant string (e.g., "Open+Sans:regular,italic,700")
                $family[] = urlencode( $font . ':' . join( ',', $this->choose_google_font_variants( $font, $allowed_fonts[ $font ]['variants'] ) ) );
            }
        }

        // Start the request
        $request = '';
        $uri_base = '//fonts.googleapis.com/css';

        // Convert from array to string
        if ( ! empty( $family ) ) {
            $request = add_query_arg( 'family', implode( '|', $family ), $uri_base );
        }

        // Load the font subset
        $subset = listify_theme_mod( 'typography-font-subset' );

        if ( 'all' === $subset ) {
            $subsets_available = $this->get_google_font_subsets();

            // Remove the all set
            unset( $subsets_available['all'] );

            // Build the array
            $subsets = array_keys( $subsets_available );
        } else {
            $subsets = array(
                $subset,
            );

            if ( 'latin' !== $subset ) {
                $subsets[] = 'latin';
            }
        }

        // Append the subset string
        if ( '' !== $request && ! empty( $subsets ) ) {
            $request = add_query_arg( 'subset', join( ',', $subsets ), $request );
        }

        return apply_filters( 'listify_get_google_font_uri', $request );
    }

    public function choose_google_font_variants( $font, $variants = array() ) {
        $chosen_variants = array();
        if ( empty( $variants ) ) {
            $fonts = listify_get_google_fonts();

            if ( array_key_exists( $font, $fonts ) ) {
                $variants = $fonts[ $font ]['variants'];
            }
        }

        // If a "regular" variant is not found, get the first variant
        if ( ! in_array( 'regular', $variants ) ) {
            $chosen_variants[] = $variants[0];
        } else {
            $chosen_variants[] = 'regular';
        }

        // Only add "italic" if it exists
        if ( in_array( 'italic', $variants ) ) {
            $chosen_variants[] = 'italic';
        }

        // Only add "700" if it exists
        if ( in_array( '700', $variants ) ) {
            $chosen_variants[] = '700';
        }

        return apply_filters( 'listify_font_variants', array_unique( $chosen_variants ), $font, $variants );
    }

    public function get_google_font_subsets() {
        return apply_filters( 'listify_get_google_font_subsets', array(
            'all'          => __( 'All', 'listify' ),
            'arabic'       => __( 'Arabic', 'listify' ),
            'bengali'      => __( 'Bengali', 'listify' ),
            'cyrillic'     => __( 'Cyrillic', 'listify' ),
            'cyrillic-ext' => __( 'Cyrillic Extended', 'listify' ),
            'devanagari'   => __( 'Devanagari', 'listify' ),
            'greek'        => __( 'Greek', 'listify' ),
            'greek-ext'    => __( 'Greek Extended', 'listify' ),
            'gujarati'     => __( 'Gujarati', 'listify' ),
            'hebrew'       => __( 'Hebrew', 'listify' ),
            'khmer'        => __( 'Khmer', 'listify' ),
            'latin'        => __( 'Latin', 'listify' ),
            'latin-ext'    => __( 'Latin Extended', 'listify' ),
            'tamil'        => __( 'Tamil', 'listify' ),
            'telugu'       => __( 'Telugu', 'listify' ),
            'thai'         => __( 'Thai', 'listify' ),
            'vietnamese'   => __( 'Vietnamese', 'listify' ),
        ) );
    }

    public function all_font_choices() {
        $fonts   = $this->get_all_fonts();
        $choices = array();

        foreach ( $fonts as $key => $font ) {
            $choices[ $key ] = $font['label'];
        }

        return apply_filters( 'listify_all_font_choices', $choices );
    }

    public function all_font_choices_js() {
        $fonts   = $this->get_all_fonts();
        $choices = array();

        // Repackage the fonts into value/label pairs
        foreach ( $fonts as $key => $font ) {
            $choices[] = array( 'k' => $key, 'l' => $font['label'] );
        }

        return $choices;
    }

    public function get_all_fonts() {
        $heading1       = array( 1 => array( 'label' => sprintf( '--- %s ---', __( 'Standard Fonts', 'listify' ) ) ) );
        $standard_fonts = $this->get_standard_fonts();
        $heading2       = array( 2 => array( 'label' => sprintf( '--- %s ---', __( 'Google Fonts', 'listify' ) ) ) );
        $google_fonts   = listify_get_google_fonts();

        return apply_filters( 'listify_all_fonts', array_merge( $heading1, $standard_fonts, $heading2, $google_fonts ) );
    }

    public function sanitize_font_choice( $font ) {
        if ( ! is_string( $font ) ) {
            $return = '';
        } else if ( array_key_exists( $font, $this->all_font_choices() ) ) {
            $return = $font;
        } else {
            $return = '';
        }

        return $return;
    }

    /**
     * Sanitizes the font choices and creates a stack
     */
    public function get_font_family( $font ) {
        $font = $this->sanitize_font_choice( $font );
        $all_fonts = $this->get_all_fonts();

        if ( isset( $all_fonts[ $font ][ 'stack' ] ) && ! empty( $all_fonts[ $font ][ 'stack' ] ) ) {
            $stack = $all_fonts[ $font ][ 'stack' ];
        } elseif ( in_array( $font, $this->all_font_choices() ) ) {
            $stack = '"' . $font . '", Helvetica, Arial, sans-serif';
        } else {
            $stack = 'Helvetica, Arial, sans-serif';
        }

        return $stack;
    }

    public function get_line_height( $height ) {
        if ( is_int( $height ) ) {
            return absint( $height );
        }

        return esc_attr( 'normal' );
    }

}
