<?php
/**
 * General icon methods.
 *
 * Mostly from Make
 * 
 * @see https://github.com/thethemefoundry/make
 */
class Listify_Customizer_Icons {

    public function all_icon_choices() {
        $icons   = $this->get_all_icons();
        $choices = array();

        // Repackage the fonts into value/label pairs
        foreach ( $icons as $class ) {
            $choices[] = array( 'k' => $class, 'l' => $class );
        }

        return $choices;
    }

    public function get_all_icons() {
        $icons = file_get_contents( dirname( __FILE__ ) . '/assets/ionicons/ionicons.json' );
        $icons = json_decode( $icons, true );

        $icon_list = array();

        foreach ( $icons as $icon ) {
            $icon_list[] = $icon[ 'name' ];
        }

        return $icon_list;
    }

}
