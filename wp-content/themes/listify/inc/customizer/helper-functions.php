<?php

function listify_get_theme_mod_defaults() {
    $mods = array(
        // Style Kit
        'style-kit' => 'default',

        // Navigation
        'nav-cart' => 1,
        'nav-search' => 1,
        'nav-secondary' => 1,
        'nav-megamenu' => 'job_listing_category',

        // Header
        'fixed-header' => 0,
        'fixed-header' => 0,

        // Colors - fill in the rest in `class-customizer-control-group-color-scheme.php`
        'color-scheme' => 'default',

        // Typography - fill in the rest in `class-customizer-control-group-font-kit.php`
        'font-pack' => 'montserrat',
        'typography-font-subset' => 'latin',

        // Layout
        'content-box-style' => 'default',
        'content-button-style' => 'default',
        'content-sidebar-position' => 'right',

        // Copyright
        'footer-style' => 'dark',
        'copyright-text' => sprintf( __( 'Copyright %s &copy; %s. All Rights Reserved', 'listify' ), get_bloginfo( 'name' ), date( 'Y' ) ),

        // Call to Action
        'call-to-action-display' => 1,
        'call-to-action-title' => sprintf( '%s is the best way to find & discover great local businesses', get_bloginfo( 'name' ) ),
        'call-to-action-description' => 'It just gets better and better', 
        'call-to-action-button-text' => 'Create Your Account',
        'call-to-action-button-href' => '',
        'call-to-action-button-subtext' => 'and get started in minutes',

        // As Seen On
        'as-seen-on-title' => '',
        'as-seen-on-logos' => '',
    );

    $mods = apply_filters( 'listify_theme_mod_defaults', $mods );

    return $mods;
}

function listify_theme_mod( $key ) {
    $mods = listify_get_theme_mod_defaults();

    $default = isset( $mods[ $key ] ) ? $mods[ $key ] : '';

    return get_theme_mod( $key, $default );
}

/**
 * Get information about a control group
 *
 * Can get all groups, or a specific group
 */
function listify_get_control_group( $group_id, $get = false ) {
    $group_id = str_replace( '-', '_', $group_id );

    $groups = listify_customizer()->control_groups;

    if ( ! isset( $groups->$group_id ) ) {
        return false;
    }

    if ( $get ) {
        return $groups->$group_id->groups[ $get ];
    }

    return $groups->$group_id->groups;
}

/**
 * Get the current color scheme
 *
 * Returns the controls instead of the total scheme data
 * for backwards compat
 */
function listify_get_color_scheme() {
    $current_scheme = get_theme_mod( 'color-scheme', 'default' );
    $scheme_data = listify_get_control_group( 'color_scheme', sanitize_title( $current_scheme ) );

    return $scheme_data[ 'controls' ];
}

/**
 * Get the current style kit data
 */
function listify_get_style_kit() {
    $current_kit = get_theme_mod( 'style-kit' );
    $kit_data = listify_get_control_group( 'style_kit', $current_kit );

    return $kit_data;
}

/**
 * Get the current style kit controls
 */
function listify_get_style_kit_controls() {
    $kit_data = listify_get_style_kit();

    return $kit_data[ 'controls' ];
}
