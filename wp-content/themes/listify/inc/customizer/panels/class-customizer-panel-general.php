<?php
/**
 * General Panel
 *
 * @since Listify 1.3.0
 */

class Listify_Customizer_Panel_General extends Listify_Customizer_Panel {

    public function __construct() {
        parent::__construct( array(
            'id' => 'general',
            'title' => __( 'General', 'listify' ),
            'priority' => 1
        ) );

        add_action( 'customize_register', array( $this, 'move_background_image' ), 40 );
        add_action( 'customize_register', array( $this, 'move_site_identity' ), 40 );
        add_action( 'customize_register', array( $this, 'move_header_image' ), 40 );
        add_action( 'customize_register', array( $this, 'move_front_page' ), 40 );

        $this->sections = array();
    }

    public function move_background_image( $wp_customize ) {
        $wp_customize->get_section( 'background_image' )->panel = 'general';
    }

    public function move_site_identity( $wp_customize ) {
        $wp_customize->get_section( 'title_tagline' )->panel = 'general';
        $wp_customize->get_section( 'title_tagline' )->title = __( 'Site Logo & Header', 'listify' );
    }

    public function move_header_image( $wp_customize ) {
        $wp_customize->get_control( 'header_image' )->section = 'title_tagline';
        $wp_customize->get_control( 'header_image' )->label = __( 'Site Logo', 'listify' );
    }

    public function move_front_page( $wp_customize ) {
        if ( $wp_customize->get_section( 'static_front_page' ) ) {
            $wp_customize->get_section( 'static_front_page' )->panel = 'general';
            $wp_customize->get_section( 'static_front_page' )->title = __( 'Homepage Display', 'listify' );
        }
    }
}
