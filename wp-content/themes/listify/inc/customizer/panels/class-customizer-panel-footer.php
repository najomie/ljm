<?php
/**
 * Footer Panel
 *
 * @since Listify 1.3.0
 */

class Listify_Customizer_Panel_Footer extends Listify_Customizer_Panel {

    public function __construct() {
        parent::__construct( array(
            'title' => __( 'Footer', 'listify' ),
            'id' => 'footer',
            'priority' => 5
        ) );

        $this->sections = array( 
            'call-to-action' => array(
                'title' => __( 'Call to Action', 'listify' ),
                'controls' => array(
                    'call-to-action-display' => array(
                        'label' => __( 'Display this section', 'listify' ),
                        'type' => 'checkbox'
                    ),
                    'call-to-action-title' => array(
                        'label' => __( 'Title', 'listify' )
                    ),
                    'call-to-action-description' => array(
                        'label' => __( 'Description', 'listify' ),
                        'type' => 'textarea'
                    ),
                    'call-to-action-button-text' => array(
                        'label' => __( 'Button Label', 'listify' )
                    ),
                    'call-to-action-button-href' => array(
                        'label' => __( 'Button Link', 'listify' )
                    ),
                    'call-to-action-button-subtext' => array(
                        'label' => __( 'Button Subtext', 'listify' )
                    )
                )
            ),
            'as-seen-on' => array(
                'title' => __( 'As Seen On', 'listify' ),
                'controls' => array(
                    'as-seen-on-title' => array(
                        'label' => __( 'Title', 'listify' )
                    ),
                    'as-seen-on-logos' => array(
                        'label' => __( 'Logos', 'listify' ),
                        'type' => 'textarea'
                    )
                )
            ),
            'copyright' => array(
                'title' => __( 'Copyright', 'listify' ),
                'controls' => array(
                    'footer-style' => array(
                        'label' => __( 'Display Style', 'listify' ),
                        'type' => 'select',
                        'choices' => array(
                            'dark' => __( 'Dark', 'listify' ),
                            'light' => __( 'Transparent', 'listify' )
                        )
                    ),
                    'copyright-text' => array(
                        'label' => __( 'Copyright Text', 'listify' )
                    )
                )
            )
        );
    }

}
