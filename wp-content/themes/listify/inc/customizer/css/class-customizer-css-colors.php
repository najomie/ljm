<?php

class Listify_Customizer_CSS_Colors {

    public function __construct() {
        $this->css = listify_customizer()->css;
        $this->scheme = sanitize_title( listify_theme_mod( 'color-scheme', 'Default' ) );

        add_action( 'listify_output_customizer_css', array( $this, 'colors' ), 0 );
    }

    public function colors() {
        /**
         * Page Background Color
         */
        $page_background = '#' . get_background_color();

        $this->css->add( array(
            'selectors' => array(
                'html'
            ),
            'declarations' => array(
                'background-color' => esc_attr( $page_background )
            )
        ) );

        /**
         * Inputs
         */
        $input_text_color = listify_theme_mod( 'color-input-text' );
        $input_background_color = listify_theme_mod( 'color-input-background' );
        $input_border_color = listify_theme_mod( 'color-input-border' );

        $this->css->add( array(
            'selectors' => array(
                'input',
                'textarea',
                'input[type=checkbox]',
                'input[type=radio]',
                '.site select',
                '.facetwp-facet .facetwp-checkbox:before',
                '.filter_by_tag a:before',
                '.search-choice-close', 
                '.widget_layered_nav li a:before', 
                '.site-main .content-box select',
                '.site-main .job_listings select',
                'body .chosen-container-single .chosen-single',
                'body .chosen-container-multi .chosen-choices li.search-field input[type=text]',
                '.select2-container .select2-choice',
                '.entry-content div.mce-toolbar-grp',
                '.wp-editor-wrap'
            ),
            'declarations' => array(
                'color' => esc_attr( $input_text_color ),
                'border-color' => esc_attr( $input_border_color ),
                'background-color' => esc_attr( $input_background_color )
            )
        ) );

        $this->css->add( array(
            'selectors' => array(
                'input[type=checkbox]:checked:before',
                '.facetwp-facet .facetwp-checkbox.checked:after',
                '.facetwp-facet .facetwp-link.checked',
            ),
            'declarations' => array(
                'color' => esc_attr( $input_text_color ),
            )
        ) );

        $this->css->add( array(
            'selectors' => array(
                'input:focus',
                'textarea:focus',
            ),
            'declarations' => array(
                'background-color' => esc_attr( $this->css->darken( $input_background_color, 10 ) )
            )
        ) );

        // empty covers
        $this->css->add( array(
            'selectors' => array(
                '.listing-cover',
                '.entry-cover',
                '.homepage-cover.page-cover',
                '.list-cover',
                '.payment_methods li'
            ),
            'declarations' => array(
                'background-color' => $this->css->darken( esc_attr( $page_background ), -10 )
            )
        ) );

        /**
         * Body Text Color
         *
         * A lot of the specific selectors are to override plugin CSS
         * or links that stick out too much (buttons, etc).
         */
        $body_text_color = listify_theme_mod( 'color-body-text' );

        $this->css->add( array(
            'selectors' => array(
                'body',
                '.listify_widget_panel_listing_tags .tag',
                '.entry-cover.no-image',
                '.entry-cover.no-image a',
                '.listing-cover.no-image',
                '.listing-cover.no-image a:not(.button)',
                '.button[name="apply_coupon"]',
                '.button[name="apply_coupon"]:hover',
                '.widget a',
                '.content-pagination .page-numbers',
                '.facetwp-pager .facetwp-page',
                '.type-job_listing.style-list .job_listing-entry-header',
                '.type-job_listing.style-list .job_listing-entry-header a',
                '.js-toggle-area-trigger',
                '.job-dashboard-actions a',
                'body.fixed-map .site-footer',
                'body.fixed-map .site-footer a',
                '.homepage-cover .job_search_form .select:after',
                '.tabbed-listings-tabs a',
                '.archive-job_listing-toggle',
                '.job-manager-form fieldset.fieldset-job_hours',
                '.no-image .ion-ios-star:before',
                '.no-image .ion-ios-star-half:before',
                '.select2-default',
                '.select2-container .select2-choice',
                '.select2-container-multi .select2-choices .select2-search-choice',
                '.filter_by_tag a',
                'a.upload-images',
                'a.upload-images span',
                '.nav-menu .sub-menu.category-list a',
                '.woocommerce-tabs .tabs a',
                '.job-manager-bookmark-actions a',
                '.star-rating-wrapper a:hover ~ a:before',
                '.star-rating-wrapper a:hover:before',
                '.star-rating-wrapper a.active ~ a:before',
                '.star-rating-wrapper a.active:before',
                '.cluster-overlay a',
                '.map-marker-info',
                '.map-marker-info a',
                '.archive-job_listing-layout.button.active',
            ),
            'declarations' => array(
                'color' => esc_attr( $body_text_color )
            )
        ) );

        // slightly lighter body text to stand out against body text
        $this->css->add( array(
            'selectors' => array(
                '.comment-meta a',
                '.commentlist a.comment-ago',
                'div:not(.no-image) .star-rating:before',
                'div:not(.no-image) .stars span a:before',
                '.job_listing-author-descriptor',
                '.entry-meta',
                '.entry-meta a',
                '.home-widget-description',
                '.listings-by-term-content .job_listing-rating-count',
                '.listings-by-term-more a',
                '.search-form .search-submit:before',
                '.mfp-content .mfp-close:before',
                'div:not(.job-package-price) .woocommerce .amount',
                '.woocommerce .quantity',
                '.showing_jobs',
                '.account-sign-in',
                '.archive-job_listing-layout.button',
            ),
            'declarations' => array(
                'color' => $this->css->darken( esc_attr( $body_text_color ), 35 )
            )
        ) );

        $this->css->add( array(
            'selectors' => array(
                '.social-profiles a',
                '.listing-gallery-nav .slick-dots li button:before'
            ),
            'declarations' => array(
                'background-color' => $this->css->darken( esc_attr( $body_text_color ), 35 )
            )
        ) );

        // ultra dark popup needs a standard gray
        if ( in_array( $this->scheme, array( 'ultra-dark' ) ) ) {
            $this->css->add( array(
                'selectors' => array(
                    '.popup',
                    '.mfp-content .mfp-close:before',
                    '.select:after',
                    '.homepage-cover .job_search_form .select:after',
                    '.search-form .search-submit:before',
                    '.cluster-overlay a',
                    '.map-marker-info',
                    '.map-marker-info a'
                ),
                'declarations' => array(
                    'color' => '#454545'
                )
            ) );

            $this->css->add( array(
                'selectors' => array(
                    'table',
                    'td',
                    'th'
                ),
                'declarations' => array(
                    'border-color' => 'rgba(255, 255, 255, .1)'
                )
            ) );
        }

        /**
         * Body Link Color
         */
        $body_link_color = listify_theme_mod( 'color-link' );

        $this->css->add( array(
            'selectors' => array(
                'a',
                '.content-pagination .page-numbers.current',
                '.listify_widget_panel_listing_content a' // since this is a widget it uses body color normally
            ),
            'declarations' => array(
                'color' => esc_attr( $body_link_color )
            )
        ) );

        // darken on hover
        $this->css->add( array(
            'selectors' => array(
                'a:active',
                'a:hover',
                '.primary-header .current-account-toggle .sub-menu a'
            ),
            'declarations' => array(
                'color' => $this->css->darken( esc_attr( $body_link_color ), -25 )
            )
        ) );

        /**
         * Header and Primary + Secondary Navigation
         *
         * It gets a bit hairy in here and should definitely be reviewed at one point
         */
        $header_background_color = listify_theme_mod( 'color-header-background' );
        $primary_navigation_text_color = listify_theme_mod( 'color-navigation-text' );
        $secondary_navigation_text_color = listify_theme_mod( 'color-secondary-navigation-text' );
        $secondary_navigation_background_color = listify_theme_mod( 'color-secondary-navigation-background' );
        $tertiary_navigation_text_color = listify_theme_mod( 'color-tertiary-navigation-text' );
        $tertiary_navigation_background_color = listify_theme_mod( 'color-tertiary-navigation-background' );

        // main header
        $this->css->add( array(
            'selectors' => array(
                '.search-overlay',
                '.primary-header'
            ),
            'declarations' => array(
                'background-color' => esc_attr( $header_background_color )
            )
        ) );

        // mega menu on large devices
        if ( ! in_array( $this->scheme, array( 'default', 'green-flash', 'green' ) ) ) {
            $this->css->add( array(
                'selectors' => array(
                    '.nav-menu.secondary .sub-menu.category-list'
                ),
                'declarations' => array(
                    'background-color' => esc_attr( $header_background_color )
                ),
                'media' => 'screen and (min-width: 768px)'
            ) );
        } else {
            $this->css->add( array(
                'selectors' => array(
                    '.nav-menu.secondary .sub-menu.category-list'
                ),
                'declarations' => array(
                    'background-color' => esc_attr( $page_background )
                ),
                'media' => 'screen and (min-width: 768px)'
            ) );
        }

        // default menu links should have a color, but this gets overwritten down the line
        // sub menu link items are the same as the `$header_background_color` as well
        $this->css->add( array(
            'selectors' => array(
                '.nav-menu a',
                '.nav-menu li:before',
                '.nav-menu li:after',
                '.nav-menu a:before',
                '.nav-menu a:after',
                '.nav-menu ul a',
                '.nav-menu.primary ul ul a',
                '.nav-menu.primary ul ul li:before',
                '.nav-menu.primary ul ul li:after',
            ),
            'declarations' => array(
                'color' => esc_attr( $header_background_color )
            )
        ) );

        // set the primary navigation text color on large devices
        $this->css->add( array(
            'selectors' => array(
                '.nav-menu.primary a',
                '.nav-menu.primary li:before',
                '.nav-menu.primary li:after',
                '.nav-menu.primary a:before',
                '.nav-menu.primary a:after',
            ),
            'declarations' => array(
                'color' => esc_attr( $primary_navigation_text_color )
            ),
            'media' => 'screen and (min-width: 992px)'
        ) );

        // close button on search overlay needs to stand off header (like navigation text)
        $this->css->add( array(
            'selectors' => array(
                '.search-overlay a.search-overlay-toggle',
            ),
            'declarations' => array(
                'color' => esc_attr( $primary_navigation_text_color )
            )
        ) );

        // set the secondary navigation background color
        $this->css->add( array(
            'selectors' => array(
                '.main-navigation',
            ),
            'declarations' => array(
                'background-color' => esc_attr( $secondary_navigation_background_color )
            )
        ) );

        // set the secondary navigation top level links
        $this->css->add( array(
            'selectors' => array(
                '.nav-menu.secondary > li > a',
                '.nav-menu.secondary > li > a:before',
                '.nav-menu.secondary > li > a:after',
                '.nav-menu.secondary > li:before',
                '.nav-menu.secondary > li:after'
            ),
            'declarations' => array(
                'color' => esc_attr( $secondary_navigation_text_color )
            ),
            'media' => 'screen and (min-width: 992px)'
        ) );

        $this->css->add( array(
            'selectors' => array(
                '.nav-menu .children.category-list .container:before',
                '.nav-menu .sub-menu.category-list .container:before',
                'ul.nav-menu .children.category-list .container:before',
                'ul.nav-menu .sub-menu.category-list .container:before'
            ),
            'declarations' => array(
                'border-top-color' => esc_attr( $secondary_navigation_background_color )
            ),
            'media' => 'screen and (min-width: 992px)'
        ) );

        // set the tertiary navigation background color
        $this->css->add( array(
            'selectors' => array(
                '.tertiary-navigation'
            ),
            'declarations' => array(
                'background-color' => esc_attr( $tertiary_navigation_background_color )
            )
        ) );

        // set the secondary navigation top level links
        $this->css->add( array(
            'selectors' => array(
                '.nav-menu.tertiary > ul > li > a',
                '.nav-menu.tertiary > li > a',
                '.nav-menu.tertiary > li > a:before',
                '.nav-menu.tertiary > li > a:after',
                '.nav-menu.tertiary > li:before',
                '.nav-menu.tertiary > li:after'
            ),
            'declarations' => array(
                'color' => esc_attr( $tertiary_navigation_text_color )
            ),
            'media' => 'screen and (min-width: 992px)'
        ) );

        // see above, with no @media
        $this->css->add( array(
            'selectors' => array(
                '.navigation-bar-toggle',
                '.search-overlay-toggle'
            ),
            'declarations' => array(
                'color' => esc_attr( $secondary_navigation_text_color )
            )
        ) );

        // dark color scheme has some special things
        if ( in_array( $this->scheme, array( 'dark', 'light-gray' ) ) ) {
            // mega menu, and cta
            $this->css->add( array(
                'selectors' => array(
                    '.nav-menu.secondary .sub-menu.category-list',
                ),
                'declarations' => array(
                    'background-color' => esc_attr( $header_background_color )
                ),
                'media' => 'screen and (min-width: 768px)'
            ) );

            // dropdown arrow
            $this->css->add( array(
                'selectors' => array(
                    'ul.nav-menu .sub-menu.category-list .container:before'
                ),
                'declarations' => array(
                    'border-top-color' => esc_attr( $secondary_navigation_background_color )
                )
            ) );
        }

        // light scheme has a white header so the text needs to be updated
        if ( in_array( $this->scheme, array( 'radical-red', 'iced-coffee', 'light-gray' ) ) ) {
            $this->css->add( array(
                'selectors' => array(
                    '.nav-menu a',
                    '.nav-menu li:before',
                    '.nav-menu li:after',
                    '.nav-menu a:before',
                    '.nav-menu a:after',
                    '.nav-menu ul a',
                    '.nav-menu.primary ul ul a',
                    '.nav-menu.primary ul ul li:before',
                    '.nav-menu.primary ul ul li:after',
                ),
                'declarations' => array(
                    'color' => esc_attr( $primary_navigation_text_color )
                )
            ) );
        }

        /**
         * Primary color used for buttons, and important things
         */
        $primary = listify_theme_mod( 'color-primary' );

        $this->css->add( array(
            'selectors' => array(
                '.listify_widget_panel_listing_tags .tag.active:before',
                '.job-package-includes li:before',
                '.woocommerce-tabs .tabs .active a',
                'body:not(.facetwp) .locate-me:before',
            ),
            'declarations' => array(
                'color' => esc_attr( $primary )
            )
        ) );

        $this->css->add( array(
            'selectors' => array(
                'input[type="button"].facetwp-reset:hover',
                'input[type="button"].facetwp-reset:focus',
                '.tabbed-listings-tabs a:hover',
                '.tabbed-listings-tabs a.active',
                '.archive-job_listing-toggle.active',
                'body:not(.facetwp) .locate-me:hover:before'
            ),
            'declarations' => array(
                'color' => esc_attr( $this->css->darken( $primary, -35 ) )
            )
        ) );

        $this->css->add( array(
            'selectors' => array(
                'button:not([role="presentation"])',
                'input[type="button"]',
                'input[type="reset"]',
                'input[type="submit"]',
                '.button',
                '.facetwp-type-slider .noUi-connect',
                '.ui-slider .ui-slider-range',
                '.listing-owner',
                '.comment-rating',
                '.job_listing-rating-average',
                '.map-marker.active:after',
                '.cluster',
                '.widget_calendar tbody a',
                '.job_listing-author-info-more a:first-child',
                '.load_more_jobs',
            ),
            'declarations' => array(
                'background-color' => esc_attr( $primary )
            )
        ) );

        $this->css->add( array(
            'selectors' => array(
                'button:not([role="presentation"]):hover',
                'button:not([role="presentation"]):focus',
                'input[type="button"]:hover',
                'input[type="button"]:focus',

                'input[type="reset"]:hover',
                'input[type="reset"]:focus',
                'input[type="submit"]:hover',
                'input[type="submit"]:focus',
                '.button:hover',
                '.button:focus',
                '::selection',
                '.load_more_jobs:hover',
                '.update_results.refreshing'
            ),
            'declarations' => array(
                'background-color' => esc_attr( $this->css->darken( $primary, -5 ) )
            )
        ) );

        $this->css->add( array(
            'selectors' => array(
                '::-moz-selection'
            ),
            'declarations' => array(
                'background-color' => esc_attr( $this->css->darken( $primary, -5 ) )
            )
        ) );

        $this->css->add( array(
            'selectors' => array(
                '.tabbed-listings-tabs a:hover',
                '.tabbed-listings-tabs a.active',
                '.archive-job_listing-toggle.active',
                'li.job-package:hover',
                '.job_listing_packages ul.job_packages li:hover',
                '.woocommerce-info',
                '.facetwp-type-slider .noUi-horizontal .noUi-handle',
                '.ui-slider .ui-slider-handle',
            ),
            'declarations' => array(
                'border-color' => esc_attr( $primary )
            )
        ) );

        $this->css->add( array(
            'selectors' => array(
                '.job_position_featured .content-box'
            ),
            'declarations' => array(
                'box-shadow' => '0 0 0 3px ' . esc_attr( $primary )
            )
        ) );

        // a colored header means the cart count needs to be white, and text the header color
        if ( in_array( $this->scheme, array( 'green-flash', 'green' ) ) ) {
            $decs = array(
                'color' => esc_attr( $header_background_color ),
                'background-color' => '#ffffff',
                'border-color' => esc_attr( $header_background_color )
            );
        } else {
            $decs = array(
                'color' => '#ffffff',
                'background-color' => esc_attr( $primary ),
                'border-color' => esc_attr( $header_background_color )
            );
        }

        $this->css->add( array(
            'selectors' => array(
                '.primary.nav-menu .current-cart .current-cart-count'
            ),
            'declarations' => $decs,
            'media' => 'screen and (min-width: 992px)'
        ) );

        /**
         * Accent color used for more subtle items
         */
        $accent = listify_theme_mod( 'color-accent' );

        $this->css->add( array(
            'selectors' => array(
                '.widget_layered_nav li.chosen a:after',
                '.widget_layered_nav li.chosen a',
                '.ion-ios-star:before',
                '.ion-ios-star-half:before',
                '.upload-images:hover .upload-area',
                '.comment-author .rating-stars .ion-ios-star',
                '.job_listing_packages ul.job_packages li label',
                '.upload-images:hover',
                '.search-choice-close:after',
				'.claimed-ribbon span:before',
				'.filter_by_tag a.active:after'
            ),
            'declarations' => array(
                'color' => esc_attr( $accent )
            )
        ) );

        $this->css->add( array(
            'selectors' => array(
                '.button-secondary',
                'input[type="button"].facetwp-reset',
                '.job_listing-author-info-more a:last-child'
            ),
            'declarations' => array(
                'background-color' => esc_attr( $accent )
            )
        ) );

        $this->css->add( array(
            'selectors' => array(
                '.button-secondary:hover',
                '.button-secondary:focus',
                'input[type="button"].facetwp-reset:hover',
                'input[type="button"].facetwp-reset:focus'
            ),
            'declarations' => array(
                'background-color' => esc_attr( $this->css->darken( $accent, -5 ) )
            )
        ) );

        $this->css->add( array(
            'selectors' => array(
                '.upload-images:hover'
            ),
            'declarations' => array(
                'border-color' => esc_attr( $accent )
            )
        ) );

        /**
         * Footer
         */

        $cta_text = listify_theme_mod( 'color-cta-text' );
        $cta_background = listify_theme_mod( 'color-cta-background' );
        $widgets_text = listify_theme_mod( 'color-footer-widgets-text' );
        $widgets_background = listify_theme_mod( 'color-footer-widgets-background' );
        $copy_text = listify_theme_mod( 'color-footer-text' );
        $copy_background = listify_theme_mod( 'color-footer-background' );

        $this->css->add( array(
            'selectors' => array(
                '.call-to-action'
            ),
            'declarations' => array(
                'color' => esc_attr( $cta_text ),
                'background-color' => esc_attr( $cta_background )
            )
        ) );

        $this->css->add( array(
            'selectors' => array(
                '.cta-description p',
                '.cta-subtext'
            ),
            'declarations' => array(
                'color' => esc_attr( $this->css->darken( $cta_text, 10 ) ),
            )
        ) );

        $this->css->add( array(
            'selectors' => array(
                '.site-footer-widgets'
            ),
            'declarations' => array(
                'color' => esc_attr( $widgets_text ),
                'background-color' => esc_attr( $widgets_background )
            )
        ) );

        $this->css->add( array(
            'selectors' => array(
                '.footer-widget',
                '.footer-widget a',
                '.footer-widget a:hover',
                '.site-social a:hover'
            ),
            'declarations' => array(
                'color' => esc_attr( $widgets_text )
            )
        ) );

        $this->css->add( array(
            'selectors' => array(
                '.site-footer',
                '.site-social a'
            ),
            'declarations' => array(
                'color' => esc_attr( $copy_text ),
            )
        ) );

        $this->css->add( array(
            'selectors' => array(
                '.site-footer'
            ),
            'declarations' => array(
                'background-color' => esc_attr( $copy_background )
            )
        ) );
    }

}
