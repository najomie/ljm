<?php

class Listify_Customizer_CSS_Marker_Appearance {

    public function __construct() {
        $this->css = listify_customizer()->css;

        add_action( 'listify_output_customizer_css', array( $this, 'markers' ) );
    }

    public function markers() {
		$terms = listify_get_terms( listify_get_top_level_taxonomy() );

        if ( is_wp_error( $terms ) ) {
            return;
        }

		$i_before = $i_after = $after = array();

        foreach ( $terms as $term ) {
            $color = listify_theme_mod( 'marker-color-' . $term->term_id );

            if ( ! $color ) {
                continue;
            }

			$this->css->add( array(
				'selectors' => array( '.map-marker.type-' . $term->term_id . ':after' ),
				'declarations' => array(
					'border-top-color' => $color
				)
			) );

			$this->css->add( array(
				'selectors' => array( '.map-marker.type-' . $term->term_id . ' i:after' ),
				'declarations' => array(
					'background-color' => $color
				)
			) );

			$this->css->add( array(
				'selectors' => array( '.map-marker.type-' . $term->term_id . ' i:before' ),
				'declarations' => array(
					'color' => $color
				)
			) );
		}

    }
}
