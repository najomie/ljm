<?php

class Listify_Customizer_CSS_Content {

    public function __construct() {
        $this->css = listify_customizer()->css;
        $this->scheme = sanitize_title( listify_theme_mod( 'color-scheme', 'Default' ) );

        add_action( 'listify_output_customizer_css', array( $this, 'content' ) );
    }

    public function content() {
        $content_style = listify_theme_mod( 'content-box-style' );
        $content_background = listify_theme_mod( 'color-content-background' );
        $content_border = listify_theme_mod( 'color-content-border' );
        $content_accent = listify_theme_mod( 'color-content-accent' );

        $decs = array(
            'background-color' => esc_attr( $content_background ),
            'box-shadow' => 'inset 0 0 0 1px ' . esc_attr( $content_border ),
            'border' => 0
        );

        if ( 'default' == $content_style ) {
            $decs[ 'box-shadow' ] = $decs[ 'box-shadow' ] . ', rgba(0, 0, 0, .03) 0 2px 0';
        } elseif ( 'minimal' == $content_style ) {
            $decs[ 'box-shadow' ] = $decs[ 'box-shadow' ];
        } elseif ( 'shadow' == $content_style ) {
            $decs[ 'box-shadow' ] = $decs[ 'box-shadow' ] . ', rgba(0, 0, 0, 0.12) 0px 1px 3px 0px';
        }

        $this->css->add( array(
            'selectors' => array(
                '.content-box',
                '.content-shop-wrapper .archive-job_listing-filters-wrapper.top.type-product',
                '.content-shop-wrapper .type-product',
                '.home-feature',
                '.job-package',
                '.job_filters',
                '.listify_widget_search_listings.home-widget .archive-job_listing-filters-wrapper.top.job_search_form',
                '.listify_widget_search_listings.home-widget .job_search_form',
                '.listing-by-term-inner',
                '.single-job_listing-description',
                '.tabbed-listings-tabs a',
                '.tabbed-listings-tabs a.archive-job_listing-filters-wrapper.top',
                '.type-product .thumbnails a',
                '.type-product .thumbnails a.archive-job_listing-filters-wrapper.top',
                '.widget',
                '.woocommerce div.product div.archive-job_listing-filters-wrapper.top.summary',
                '.woocommerce div.product div.summary',
                '.woocommerce-main-image',
                '.woocommerce-page div.product div.archive-job_listing-filters-wrapper.top.summary',
                '.woocommerce-page div.product div.summary, .woocommerce-tabs',
                '.archive-job_listing-layout.button',
                '.nav-menu .children.category-list .category-count',
                '.nav-menu .sub-menu.category-list .category-count',
                'ul.nav-menu .children.category-list .category-count',
                'ul.nav-menu .sub-menu.category-list .category-count',
                '.facetwp-pager .facetwp-page',
                '.job-manager-pagination li a',
                '.job-manager-pagination li span',
                '.js-toggle-area-trigger',
                '.site .facetwp-sort select',
                'a.page-numbers, span.page-numbers',
                '.archive-job_listing-toggle-inner'
            ),
            'declarations' => $decs
        ) );

        /**
         * Accent
         */

        $this->css->add( array(
            'selectors' => array(
                '.comment-reply-title',
                '.entry-content .rcp_form .rcp_subscription_fieldset .rcp_subscription_message', 
                '.entry-content .rcp_header',
                '.entry-content h2',
                '.entry-content h3',
                '.entry-content h4',
                '.job-manager-form h2',
                '.job_listing_packages ul.job_packages .package-section',
                '.listify_widget_panel_listing_content h2',
                '.listify_widget_panel_listing_content h3',
                '.listify_widget_panel_listing_content h4',
                '.listing-by-term-title',
                '.widget-title',
                '.woocommerce-account .woocommerce legend',
                '.woocommerce-tabs .tabs a',
                '.account-sign-in',
                '.job-manager-form fieldset.fieldset-job_hours',
                '.ninja-forms-required-items',
                '.showing_jobs',
                '.summary .stock',
                '.woocommerce-tabs .woocommerce-noreviews',
            ),
            'declarations' => array(
                'border-color' => esc_attr( $content_accent )
            )
        ) );

        $this->css->add( array(
            'selectors' => array(
                '.ui-slider',
                '.ui-slider-range',
                '.site .widget select'
            ),
            'declarations' => array(
                'background-color' => esc_attr( $content_accent )
            )
        ) );

        if ( in_array( $this->scheme, array( 'iced-coffee' ) ) ) {
            $this->css->add( array(
                'selectors' => array(
                    '.nav-menu .children.category-list .category-count',
                    '.nav-menu .sub-menu.category-list .category-count',
                    'ul.nav-menu .children.category-list .category-count',
                    'ul.nav-menu .sub-menu.category-list .category-count',
                ),
                'declarations' => array(
                    'box-shadow' => 'none',
                    'border' => '1px solid ' . esc_attr( $content_accent )
                )
            ) );
        }
    }

}
