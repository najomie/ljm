<?php

class Listify_Customizer_CSS_Nav {

    public function __construct() {
        $this->css = listify_customizer()->css;

        add_action( 'listify_output_customizer_css', array( $this, 'nav' ) );
    }

    public function nav() {
        $display_secondary = listify_theme_mod( 'nav-secondary' );

        if ( ! $display_secondary ) {
            $this->css->add( array(
                'selectors' => array(
                    '.main-navigation'
                ),
                'declarations' => array(
                    'display' => 'none'
                ),
                'media' => 'screen and (min-width: 992px)'
            ) );

            $this->css->add( array(
                'selectors' => array(
                    'body.fixed-map .job_listings-map-wrapper'
                ),
                'declarations' => array(
                    'top' => '75px'
                ),
                'media' => 'screen and (min-width: 992px)'
            ) );
        }
    }

}
