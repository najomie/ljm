<?php
/**
 * Customize
 *
 * @package Listify
 * @since Listify 1.0.0
 */

class Listify_Customizer {

    private static $instance;

    public $panels;
    public $fonts;
    public $icons;
    public $output;
    public $control_groups;

    public static function instance() {
        if ( ! isset( self::$instance ) && ! ( self::$instance instanceof Listify_Customizer ) ) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    public function __construct() {
        $files = array(
            'helper-functions.php',

            // assets
            'assets/google-fonts/google-fonts.php',

            // customizer
            'class-customizer-fonts.php',
            'class-customizer-icons.php',

            'class-customizer-priority.php',
            'class-customizer-css.php',
            'class-customizer-control-groups.php',
            'class-customizer-control.php',
            'class-customizer-section.php',
            'class-customizer-panel.php',
            'class-customizer-panels.php',
            'class-customizer-output-css.php'
        );

        foreach ( $files as $file ) {
            include_once( trailingslashit( dirname( __FILE__) ) . $file );
        }

        add_action( 'init', array( $this, 'customizer' ), 9 );
        add_action( 'customize_register', array( $this, 'custom_controls' ), 8 );
        add_action( 'customize_controls_enqueue_scripts', array( $this, 'customizer_scripts' ) );
    }

    public function customizer() {
        $this->panels = new Listify_Customizer_Panels();
        $this->control_groups = new Listify_Customizer_Control_Groups();
        $this->output = new Listify_Customizer_Output();
        $this->fonts = new Listify_Customizer_Fonts();
        $this->icons = new Listify_Customizer_Icons();
        $this->css = new Listify_Customizer_CSS();
    }

    public function custom_controls() {
        $controls = array(
            'class-customizer-control-group.php',
            'class-customizer-control-color-scheme.php',
            'class-customizer-control-style-kit.php',
            'class-customizer-control-font-pack.php',
            'class-customizer-control-description.php',
        );

        foreach ( $controls as $file ) {
            include_once( trailingslashit( dirname( __FILE__) ) . 'control/' . $file );
        }
    }

    public function customizer_scripts() {
        wp_enqueue_script( 'listify-customizer-admin', get_template_directory_uri() . '/inc/customizer/assets/js/customizer-admin.js', array( 'jquery', 'chosen' ), time(), true );

        wp_register_script( 'chosen', get_template_directory_uri() . '/inc/customizer/assets/js/chosen/chosen.jquery.min.js', array( 'jquery' ) );
        wp_register_style( 'chosen', get_template_directory_uri() . '/inc/customizer/assets/js/chosen/chosen.min.css' );

        wp_enqueue_script( 'chosen' );
        wp_enqueue_style( 'chosen' );

        $data = apply_filters( 'listify_customizer_scripts_data', array(
            'chosen' => array(
                'no_results_found' => __( 'No results found.', 'listify' ),
                'placeholder' => __( 'Select an Option', 'listify' )
            )
        ) );

        wp_localize_script( 'listify-customizer-admin', 'listifyCustomizerOptions', $data );
    }

}

function listify_customizer() {
    return Listify_Customizer::instance();
}

listify_customizer();
