<?php get_header(); ?>



<?php
global $post;
$term =	$wp_query->queried_object;
$category = $term->slug;

?>
<section class="content container blog-container">

   <aside class="type-menu">
		<?php $terms = get_terms( 'category', array( 'parent' => 0,'hide_empty' => 1,) ); ?>
		<ul>
         <?php
            $blog = get_option('page_for_posts');
            if( is_home() ): $isblog = 'current'; else: $isblog = 'tax'; endif;
            echo '<li class="'.$isblog.'"><a class="'.$isblog.'" href="' . $blog . '"><span>' . 'Toutes les nouvelles' . '</span></a></li>';
          ?>
		   <?php foreach ( $terms as $term ):
   			$term_link = get_term_link( $term );
   			$cat = get_query_var('cat');
   			$current_cat = get_category($cat);

   			if( !empty($cat) && $term->slug == $current_cat->slug || !empty($cat) && $term->term_id == $current_cat->parent ): $class = 'current tax'; else: $class = 'tax'; endif;
   			echo '<li class="'.$class.'"><a class="'.$class.'" href="' . esc_url( $term_link ) . '"><span>' . $term->name . '</span></a></li>';
   		endforeach; ?>
		</ul>
	</aside>

	<div class="post-list row">

      <?php echo do_shortcode('[ajax_load_more post_type="post" category="'.$category.'" button_label="+" posts_per_page="4" max_pages="0" images_loaded="true"]'); ?>

	</div>

</section>

<?php get_footer();
