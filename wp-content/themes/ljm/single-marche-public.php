<?php get_header(); ?>


<?php

$short_desc = get_field('short_desc');
$date       = get_field('date');
$address    = get_field('address');
$schedule   = get_field('schedule');

$gallery    = get_field('gallery');
$button     = get_field('button');
$related    = get_field('related_post');


?>

   <div <?php echo apply_filters( 'listify_cover', 'entry-header entry-cover' ); ?>>
      <h1 class="page-title cover-wrapper"><?php the_title(); ?></h1>
	</div>

    <div id="primary" class="container">
        <div class="row content-area">

            <main id="main" class="site-main col-xs-12" role="main">

               <div class="row">
                  <div class="col-sm-5 gallery">
                     <?php if( $gallery ): ?>
                        <div class="slides main">
                             <?php foreach( $gallery as $img ): ?>
                                <?php
                                    $size = 'large';
                                   	$thumb = $img['sizes'][ $size ];
                                 ?>
                                 <div class="slide">
                                     <figure style="background-image:url(<?php echo $img['url']; ?>);"></figure>
                                 </div>
                             <?php endforeach; ?>
                         </div>
                        <div class="slides-nav">
                             <?php foreach( $gallery as $img ): ?>
                                <?php
                                    $size = 'thumbnail';
                                   	$thumb = $img['sizes'][ $size ];
                                 ?>
                                 <div class="slide">
                                     <img src="<?php echo $thumb; ?>" />
                                 </div>
                             <?php endforeach; ?>
                         </div>
                     <?php endif; ?>
                  </div>
                  <div class="col-sm-7 infos">

                     <section>
                        <div class="dates">
                           <?php if( $date ): ?>
                              <span><?php echo $date; ?></span>
                           <?php endif; ?>

                           <?php if( $address ): ?>
                              <span><?php echo $address; ?></span>
                           <?php endif; ?>
                        </div>

                        <div class="schedule">
                           <?php if( $schedule ): ?>
                              <span><?php echo $schedule; ?></span>
                           <?php endif; ?>
                        </div>

                     </section>

                     <?php if( $short_desc ): ?>
                        <?php echo $short_desc; ?>
                     <?php endif; ?>

                  </div>
               </div>

               <div class="row">
                  <div class="col-sm-12">

                     <?php if( $button ): ?>

                        <a href="<?php echo get_permalink($related[0]->ID); ?>" class="big-cta"><?php echo $button; ?> <i class="fa fa-angle-right"></i></a>
                     <?php endif; ?>
                  </div>
               </div>
            </main>

        </div>
    </div>

<?php get_footer(); ?>
