<?php get_header(); ?>

<section class="content container">

   <aside class="type-menu">
		<?php $terms = get_terms( 'type', array( 'parent' => 0,'hide_empty' => 1,) ); ?>
		<ul>
		   <?php foreach ( $terms as $term ):
   			$term_link = get_term_link( $term );
   			$cat = get_query_var('cat');
   			$current_cat = get_category($cat);

   			if( !empty($cat) && $term->slug == $current_cat->slug || !empty($cat) && $term->term_id == $current_cat->parent ): $class = 'current tax'; else: $class = 'tax'; endif;
   			echo '<li class="'.$class.'"><a class="'.$class.'" href="' . esc_url( $term_link ) . '"><span>' . $term->name . '</span></a></li>';
   		endforeach; ?>
		</ul>
	</aside>

	<div class="recettes-list row">

      <?php echo do_shortcode('[ajax_load_more post_type="recette" button_label="+" posts_per_page="6" max_pages="0" images_loaded="true"]'); ?>

	</div>

</section>

<?php get_footer();
