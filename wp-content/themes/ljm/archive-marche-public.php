<?php get_header(); ?>

<section class="content container">

	<div class="marche recettes-list row">

      <?php echo do_shortcode('[ajax_load_more repeater="template_1" post_type="marche-public" button_label="+" posts_per_page="6" max_pages="0" images_loaded="true"]'); ?>

	</div>

</section>

<?php get_footer();
