<?php get_header(); ?>

<?php
global $post;
$term =	$wp_query->queried_object;
$category = $term->slug;

?>

<section class="content container">

   <aside class="type-menu">
		<?php $terms = get_terms( 'type', array( 'parent' => 0,'hide_empty' => 1,) ); ?>
		<ul>
		   <?php foreach ( $terms as $term ):
   			$term_link = get_term_link( $term );
   			$cat = get_query_var('cat');

   			if( $term->slug == $category ):  $class = 'current tax'; else: $class = 'tax'; endif;
   			echo '<li class="'.$class.'"><a class="'.$class.'" href="' . esc_url( $term_link ) . '"><span>' . $term->name . '</span></a></li>';
   		endforeach; ?>
		</ul>
	</aside>

	<div class="recettes-list row">

      <?php echo do_shortcode('[ajax_load_more post_type="recette" taxonomy="type" taxonomy_terms="'.$category.'" taxonomy_operator="IN" button_label="+" posts_per_page="6" max_pages="0" images_loaded="true"]'); ?>

	</div>

</section>

<?php get_footer();
