<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Listify
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="hfeed site">

	<header id="masthead" class="site-header" role="banner">
		<div class="primary-header">
			<div class="container">
				<div class="primary-header-inner">
					<!-- <div class="site-branding">
						<?php $header_image = get_header_image(); ?>
						<?php if ( ! empty( $header_image ) ) : ?>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home" class="custom-header"><img src="<?php echo esc_url( $header_image ); ?>" alt=""></a>
						<?php endif; ?>

						<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
						<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
					</div> -->

					<div class="primary nav-menu">
						<?php
							wp_nav_menu( array(
								'theme_location' => 'primary',
								'container_class' => 'nav-menu-container'
							) );
						?>
					</div>
				</div>

				<!-- <?php if ( ! listify_has_integration( 'facetwp' ) && listify_theme_mod( 'nav-search' ) ) : ?>
				<div id="search-header" class="search-overlay">
					<div class="container">
						<?php locate_template( array( 'searchform-header.php', 'searchform.php' ), true, false ); ?>
						<a href="#search-header" data-toggle="#search-header" class="ion-close search-overlay-toggle"></a>
					</div>
				</div>
            <?php endif; ?> -->
			</div>
		</div>

		<nav>
			<div class="container">
				<div class="ljm-menu-wrapper">
					<?php
						wp_nav_menu( array(
							 'theme_location' => 'tertiary',
							 'container_class' => 'nav-menu',
							 'menu_class' => 'ljm-menu nav-menu'
						) );
					?>
				</div>
			</div>
		</nav>
	</header><!-- #masthead -->

	<?php do_action( 'listify_content_before' ); ?>

	<div id="content" class="site-content">

		<?php if( get_field('remove_bg_banner') != true && !is_page_template('templates/contact.php') ): ?>
			<?php if( get_field('page_banner') ):  $img = get_field('page_banner'); else: $img = get_default_header_image(); endif; ?>
			<div class="page-banner" style="background-image: url(<?php echo $img; ?>);">
			<div class="half-bg"></div>
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<a class="home" href="<?php echo site_url(); ?>"><img class="ljm-logo" src="<?php echo THEME_URI .'/images/logo.png'; ?>" alt="" /></a>
					</div>
					<div class="col-sm-6"></div>
				</div>
			</div>
			</div>
		<?php endif; ?>

		<?php if( is_page_template('templates/contact.php') ): ?>
			<?php echo do_shortcode('[google_map]'); ?>
		<?php endif; ?>
