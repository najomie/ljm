	</div><!-- #content -->

	<div class="footer-wrapper">

		<footer class="site-footer-widgets">
			<div class="container">
				<div class="row">

					<div class="footer-widget-column col-sm-6">
						<?php dynamic_sidebar( 'widget-area-footer-1' ); ?>
					</div>

					<div class="col-sm-1"></div>

					<div class="footer-widget-column col-sm-5">
						<?php dynamic_sidebar( 'widget-area-footer-2' ); ?>
					</div>

				</div>
			</div>
		</footer>

		<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-12 years">
						© <?php echo date('Y'); ?> Laurentides j'en manges
					</div>
					<div class="col-md-6 col-sm-12 naj">
						<a class="najomie" href="http://www.najomie.com" target="_blank">Najomie.com | Branding + Web</a>
					</div>
				</div>
			</div>
		</footer><!-- #colophon -->

	</div>

</div><!-- #page -->

<div id="ajax-response"></div>

<?php wp_footer(); ?>

</body>
</html>
