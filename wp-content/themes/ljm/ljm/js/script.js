jQuery(document).ready(function($) {

	// Slick Slider
	if ( $.fn.slick ) {

		 $('.newest-posts').slick({
			  pauseOnHover: false,
			  slidesToShow: 3,
			  slidesToScroll: 1,
			  arrows: true,
			  fade: false,
			  draggable: true,
			  swipe: true,
			  autoplay: true,
           prevArrow: '.ion-chevron-left',
           nextArrow: '.ion-chevron-right',
			  autoplaySpeed: 4000
		  });
	 }

});
