<?php

function listify_child_styles() {
    wp_enqueue_style( 'listify-child', get_stylesheet_uri() );
    wp_enqueue_script( 'slick', get_stylesheet_directory_uri() . '/js/slick.min.js', array( 'jquery' ), '1.0', true );
    wp_enqueue_script( 'script', get_stylesheet_directory_uri() . '/js/script.js', array( 'jquery' ), '1.0', true );
}
add_action( 'wp_enqueue_scripts', 'listify_child_styles', 999 );

/** Place any new code below this line */

define( 'THEME_URI', get_stylesheet_directory_uri() );
function get_default_header_image()
{
    return THEME_URI . '/images/ljm-default.jpg';
}

function get_first_image() {
    global $post, $posts;
    $first_img = '';
    ob_start();
    ob_end_clean();
    $output = preg_match_all( '/<img .+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches );
    $first_img = $matches[1][0];
    if ( empty( $first_img ) ) {

        $first_img = get_template_directory_uri() . "/images/default.jpg";
    }
    return $first_img;
}

function naj_excerpt($charlength) {
	$excerpt = get_the_excerpt();
	$excerpt = str_replace("&nbsp;","",$excerpt);
	$charlength++;

	if ( mb_strlen( $excerpt ) > $charlength ) {
		$subex = mb_substr( $excerpt, 0, $charlength - 3 );
		$exwords = explode( ' ', $subex );
		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
		if ( $excut < 0 ) {
			echo mb_substr( $subex, 0, $excut );
		} else {
			echo $subex;
		}
		echo '<span class="dots">...</span>';
	} else {
		echo $excerpt;
	}
}

/* [google-map] */
function google_map( $atts ){
   wp_enqueue_script( 'googlemap', get_stylesheet_directory_uri() . '/js/googlemap.js', array( 'jquery' ), '1.0', true );
	$js_deps[] = 'googlemap';
	wp_localize_script( 'googlemap', 'NAJ_map',
		array(
			'icon' => THEME_URI . '/images/marker.png',
		)
	);
	ob_start(); ?>

	<div id='map_canvas'></div>
	<?php return ob_get_clean();
}
add_shortcode( 'google_map', 'google_map' );

/*** [gridnews] ***/
function gridnews( $atts ){
   ob_start();
	  get_template_part('includes/grid-news');
   return ob_get_clean();
}
add_shortcode( 'gridnews', 'gridnews' );

/*** [ljm_blocs] ***/
function ljm_blocs( $atts ){
   ob_start();
	  get_template_part('includes/ljm-blocs');
   return ob_get_clean();
}
add_shortcode( 'ljm_blocs', 'ljm_blocs' );

if( function_exists('acf_add_options_page') ) {

 	// add parent
	$parent = acf_add_options_page(array(
		'page_title' 	=> 'Options LJM',
		'menu_title' 	=> 'Options LJM',
      'icon_url' => 'dashicons-welcome-widgets-menus',
		'redirect' 		=> false
	));
}
