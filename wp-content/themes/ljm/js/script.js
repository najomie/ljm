jQuery(document).ready(function($) {

	// Slick Slider
	if ( $.fn.slick ) {

		 $('.newest-posts').slick({
			  pauseOnHover: false,
			  slidesToShow: 3,
			  slidesToScroll: 1,
			  arrows: true,
			  fade: false,
			  draggable: true,
			  swipe: true,
			  autoplay: true,
           prevArrow: '.ion-chevron-left',
           nextArrow: '.ion-chevron-right',
			  autoplaySpeed: 4000
		  });

		  $('.slides').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			fade: true,
			asNavFor: '.slides-nav'
		});

		$('.slides-nav').slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			arrows: false,
			asNavFor: '.slides',
			dots: false,
			centerMode: false,
			focusOnSelect: true
		});
	 }

});
