<?php global $naj_functions; ?>
<div class="npost-wrap">
   <div class="npost-inner">
   <i class="icon ion-chevron-left"></i>
   <i class="icon ion-chevron-right"></i>
   <div class="post-list newest-posts row">
       <?php

           $args = array(
               "orderby" => "date",
               'post_status' => 'publish',
               'posts_per_page' => -1,
               "order"   => "DESC"
           );
           $wp_query = new WP_Query($args);

       ?>
       <?php if ($wp_query->have_posts()): $count = 0; while ($wp_query->have_posts()) : $wp_query->the_post(); $count ++; ?>
           <div class="col-sm-6 post-item event">
               <?php if( has_post_thumbnail() ): $src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false ); ?>
                   <figure class="post-thumbnail" style="background-image: url(<?php echo $src[0]; ?>);"></figure>
               <?php endif; ?>
               <div class="recent-meta">
                  <div class="inner-meta">
                     <?php naj_excerpt(240);?>
                     <a class="more" href="<?php echo get_permalink(); ?>">Lire la suite  →</a>
                  </div>
               </div>
           </div>
       <?php endwhile; endif; wp_reset_query(); ?>
   </div>
   </div>
</div>
