<?php global $par_functions; ?>
<?php if ( have_rows('ljm_blocs', 'options') ) : $count = ""; ?>
    <div class="ljm-blocs row">
        <?php while ( have_rows('ljm_blocs', 'options') ) : the_row(); $count ++; ?>
            <?php
                $image = get_sub_field('image');
                $color  = get_sub_field('bloc_color');
                $ftitle = get_sub_field('bloc_ftitle');
                $stitle  = get_sub_field('bloc_stitle');
                $link  = get_sub_field('bloc_lien');
            ?>
            <div class="col-sm-6 ljm-bloc match">
                 <a href="<?php echo $link; ?>">
                    <div class="bloc-c" style="background-color: <?php echo $color; ?>;"></div>
                     <div class="bloc-bg" style="background-image: url('<?php echo $image; ?>');"></div>
                     <div class="inner-bloc">
                         <h3><?php echo $ftitle; ?></h3>
                         <h4><?php echo $stitle; ?></h4>
                     </div>
                 </a>
            </div>
        <?php endwhile; ?>
    </div>
<?php endif; ?>
