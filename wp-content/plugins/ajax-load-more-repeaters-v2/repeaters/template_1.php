<article class="col-sm-4">

      	<?php if ( has_post_thumbnail() ) : ?>
      		<figure class="post-thumbnail">
      			<a href="<?php echo the_permalink(); ?>"><?php the_post_thumbnail('post-list-thumbnail'); ?>
                <h2><?php the_title(); ?></h2>
            </a>
      		</figure>
      	<?php endif; ?>

      </article>