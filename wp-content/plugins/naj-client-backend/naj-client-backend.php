<?php
/**
 * Plugin Name: Najomie backend client * Plugin URI: http://najomie.com * Description: Backend épuré pour les clients
 * Version: 1.0.0
 * Author: Gabriel Gaudreau (Najomie)
 * Author URI: http://najomie.com
 * Requires at least: 4.1
 * Tested up to: 4.1
 *
 * @package NAJ_client_backend
 * @category Core
 * @author Najomie
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'NAJ_Client_Backend' ) ) {

class NAJ_Client_Backend
{
	const DEBUG = FALSE;

	/**
	 * Plugin version, match with plugin header
	 * @var string
	 */
	public $version = '1.0.0';

	/**
	 * Use the function not the variable
	 * @var string
	 */
	public $plugin_url;

	/**
	 * Use the function not the variable
	 * @var string
	 */
	public $plugin_path;

	/**
	 * Do we update the rewrite rules for a custom post type?
	 * @var boolean
	 */
	public $flush_rules = FALSE;

	/**
	 * PLUGIN STARTUP
	 */
	public function __construct(){
		// do something when we activate/deactivate the plugin
		register_activation_hook( __FILE__, array( $this, 'activate' ) );
		register_deactivation_hook( __FILE__, array( $this, 'deactivate' ) );

		$installed = get_option( 'NAJ_Client_Backend_Version', FALSE );

		if( ! $installed or version_compare($installed, $this->version, '!=') ){
			$this->flush_rules = TRUE;
			update_option( 'NAJ_Client_Backend_Version', $this->version );
			$this->runonce();
		}		$this->hooks();
	}

	/**
	 * Runs only if plugin version changed
	 */
	public function runonce(){

		#add_filter( 'admin_client_capabilities', array($this, 'admin_client_woocommerce') );
		#add_filter( 'admin_client_capabilities', array($this, 'admin_client_aec_calendar') );
		#add_filter( 'admin_client_capabilities', array($this, 'admin_client_tribe_calendar') );
		$this->add_roles();
	}

	public function admin_client_woocommerce( $capabilities ){

		$new_caps = array(
			'manage_woocommerce'			=> TRUE,
			'view_woocommerce_reports'		=> TRUE
		);

		$capability_types = array( 'product', 'shop_order', 'shop_coupon', 'shop_webhook' );

		foreach ( $capability_types as $capability_type ) {
			$new_caps["edit_{$capability_type}"] 				= TRUE;
			$new_caps["read_{$capability_type}"] 				= TRUE;
			$new_caps["delete_{$capability_type}"] 				= TRUE;
			$new_caps["edit_{$capability_type}s"] 				= TRUE;
			$new_caps["edit_others_{$capability_type}s"] 		= TRUE;
			$new_caps["publish_{$capability_type}s"] 			= TRUE;
			$new_caps["read_private_{$capability_type}s"] 		= TRUE;
			$new_caps["delete_{$capability_type}s"] 			= TRUE;
			$new_caps["delete_private_{$capability_type}s"] 	= TRUE;
			$new_caps["delete_published_{$capability_type}s"]	= TRUE;
			$new_caps["delete_others_{$capability_type}s"] 		= TRUE;
			$new_caps["edit_private_{$capability_type}s"] 		= TRUE;
			$new_caps["edit_published_{$capability_type}s"]		= TRUE;
			$new_caps["manage_{$capability_type}_terms"] 		= TRUE;
			$new_caps["edit_{$capability_type}_terms"] 			= TRUE;
			$new_caps["delete_{$capability_type}_terms"] 		= TRUE;
			$new_caps["assign_{$capability_type}_terms"] 		= TRUE;
		}

		$capabilities += $new_caps;

		return $capabilities;
	}

	public function admin_client_aec_calendar( $capabilities ){

		$new_caps = array(
			'aec_add_events'			=> TRUE,
			'aec_manage_calendar'		=> TRUE,
			'aec_manage_events'			=> TRUE,
		);

		$capabilities += $new_caps;

		return $capabilities;
	}

	public function admin_client_tribe_calendar( $capabilities ){

		$new_caps = array(
			'delete_others_tribe_events'			=> TRUE,
			'delete_others_tribe_organizers'		=> TRUE,
			'delete_others_tribe_venues'			=> TRUE,
			'delete_private_tribe_events'			=> TRUE,
			'delete_private_tribe_organizers'		=> TRUE,
			'delete_private_tribe_venues'			=> TRUE,
			'delete_published_tribe_events'			=> TRUE,
			'delete_published_tribe_organizers'		=> TRUE,
			'delete_published_tribe_venues'			=> TRUE,
			'delete_tribe_events'					=> TRUE,
			'delete_tribe_organizers'				=> TRUE,
			'delete_tribe_venues'					=> TRUE,
			'edit_others_tribe_events'				=> TRUE,
			'edit_others_tribe_organizers'			=> TRUE,
			'edit_others_tribe_venues'				=> TRUE,
			'edit_private_tribe_events'				=> TRUE,
			'edit_private_tribe_organizers'			=> TRUE,
			'edit_private_tribe_venues'				=> TRUE,
			'edit_published_tribe_events'			=> TRUE,
			'edit_published_tribe_organizers'		=> TRUE,
			'edit_published_tribe_venues'			=> TRUE,
			'edit_tribe_events'						=> TRUE,
			'edit_tribe_organizers'					=> TRUE,
			'edit_tribe_venues'						=> TRUE,
			'publish_tribe_events'					=> TRUE,
			'publish_tribe_organizers'				=> TRUE,
			'publish_tribe_venues'					=> TRUE,
			'read_private_tribe_events'				=> TRUE,
			'read_private_tribe_organizers'			=> TRUE,
			'read_private_tribe_venues'				=> TRUE,
		);

		$capabilities += $new_caps;

		return $capabilities;
	}

	public function add_roles(){
		$capabilities = array(
			'activate_plugins'			=> FALSE,
			'create_users'				=> TRUE,
			'delete_others_pages'		=> TRUE,
			'delete_others_posts'		=> TRUE,
			'delete_pages'				=> TRUE,
			'delete_plugins'			=> FALSE,
			'delete_posts'				=> TRUE,
			'delete_private_pages'		=> TRUE,
			'delete_private_posts'		=> TRUE,
			'delete_published_pages'	=> TRUE,
			'delete_published_posts'	=> TRUE,
			'delete_themes'				=> FALSE,
			'delete_users'				=> TRUE,
			'edit_dashboard'			=> TRUE,
			'edit_files'				=> FALSE,
			'edit_others_pages'			=> TRUE,
			'edit_others_posts'			=> TRUE,
			'edit_pages'				=> TRUE,
			'edit_plugins'				=> FALSE,
			'edit_posts'				=> TRUE,
			'edit_private_pages'		=> TRUE,
			'edit_private_posts'		=> TRUE,
			'edit_published_pages'		=> TRUE,
			'edit_published_posts'		=> TRUE,
			'edit_theme_options'		=> TRUE,
			'edit_themes'				=> FALSE,
			'edit_users'				=> TRUE,
			'export'					=> TRUE,
			'import'					=> FALSE,
			'install_plugins'			=> FALSE,
			'install_themes'			=> FALSE,
			'list_users'				=> TRUE,
			'manage_categories'			=> TRUE,
			'manage_links'				=> TRUE,
			'manage_options'			=> TRUE,
			'moderate_comments'			=> FALSE,
			'promote_users'				=> TRUE,
			'publish_pages'				=> TRUE,
			'publish_posts'				=> TRUE,
			'read'						=> TRUE,
			'read_private_pages'		=> TRUE,
			'read_private_posts'		=> TRUE,
			'remove_users'				=> TRUE,
			'switch_themes'				=> FALSE,
			'unfiltered_html'			=> TRUE,
			'update_core'				=> FALSE,
			'update_plugins'			=> FALSE,
			'update_themes'				=> FALSE,
			'upload_files'				=> TRUE,
		);

		remove_role( 'admin_client' );
		add_role( 'admin_client', 'Admin client', apply_filters('admin_client_capabilities', $capabilities) );
	}

	/**
	 * Register the plugin's hooks
	 */
	public function hooks(){
		add_action( 'init', array($this, 'init'), 0 );
	}

	/**
	 * Runs on WordPress init hook
	 */
	public function init(){
		$this->flush();

		$current_user = wp_get_current_user();

		if ( in_array( 'admin_client', $current_user->roles ) ) {

			// Clean admin menu and admin bar
			add_action( 'admin_menu', array($this, 'edit_menus'), 999 );
			add_action( 'wp_before_admin_bar_render', array($this, 'remove_admin_bar_links') );

			// Hide update notices
			add_filter('pre_site_transient_update_core', array($this, 'remove_core_updates') );
			add_filter('pre_site_transient_update_plugins', array($this, 'remove_core_updates') );
			add_filter('pre_site_transient_update_themes', array($this, 'remove_core_updates') );

			// Clean dashboard
			add_action( 'wp_dashboard_setup', array($this, 'edit_dashboard_widgets') );
			add_filter( 'screen_layout_columns', array($this, 'screen_layout_columns') );
			add_filter( 'get_user_option_screen_layout_dashboard', array($this, 'screen_layout_dashboard') );
			add_filter( 'contextual_help', array($this, 'remove_dashboard_help_tab'), 999, 3 );
			add_filter( 'screen_options_show_screen', array($this, 'remove_help_tab') );
			remove_action( 'welcome_panel', 'wp_welcome_panel' );
		}

		$this->customize_login();
	}

	/**
	 * Refresh rewrite rules
	 */
	public function flush(){
		if( $this->flush_rules )
			flush_rewrite_rules();
	}

	public function activate(){
		$this->flush_rules = TRUE; // we will need to refresh the rewrite rules for the custom post types
	}

	public function deactivate(){
		$this->flush_rules = TRUE; // refresh the rewrite rules for the custom post types which are no longuer loaded
	}

	public function edit_menus(){
		remove_menu_page( 'edit-comments.php' );										// Remove the comments menu		remove_menu_page( 'tools.php' );												// Remove the tools menu
		remove_menu_page( 'themes.php' );												// Remove the themes menu
		remove_menu_page( 'options-general.php' );										// Remove the settings menu
		remove_menu_page( 'vc-general' );												// Remove the Visual Composer menu
		remove_menu_page( 'wpseo_dashboard' );											// Remove the SEO menu
		remove_menu_page( 'edit.php?post_type=acf' );									// Remove the ACF menu
		remove_menu_page( 'themepunch-google-fonts' );									// Remove the Punch Fonts menu
		remove_menu_page( 'sucuriscan' );												// Remove the Sucuri menu
		remove_menu_page( 'itsec' );													// Remove the iThemes menu
		remove_menu_page( 'edit.php?post_type=acf-field-group' );						// Remove the ACF menu
		remove_menu_page( 'wpml-translation-management/menu/translations-queue.php' );	// Remove the WPML menu

		remove_menu_page( 'mailchimp-for-wp' ); // Mailchimp Plugin
		remove_menu_page( 'sb-instagram-feed' ); // Instagram Feed

		remove_menu_page( 'pods' );
		remove_menu_page( 'pmxi-admin-import' );
		remove_menu_page( 'ajax-load-more' );
		remove_menu_page( 'envato-wordpress-toolkit' );
		remove_menu_page( 'wpcf7' );

		global $menu;
		add_menu_page( 'Menus', 'Menus', 'edit_theme_options', 'nav-menus.php', '', 'dashicons-menu', 99);	// Add Menus menu
	}

	public function remove_admin_bar_links(){
		global $wp_admin_bar;

		$wp_admin_bar->remove_menu('wp-logo');          			// Remove the WordPress logo		$wp_admin_bar->remove_menu('about');            			// Remove the about WordPress link
		$wp_admin_bar->remove_menu('wporg');            			// Remove the WordPress.org link
		$wp_admin_bar->remove_menu('documentation');    			// Remove the WordPress documentation link
		$wp_admin_bar->remove_menu('support-forums');   			// Remove the support forums link
		$wp_admin_bar->remove_menu('feedback');         			// Remove the feedback link
		$wp_admin_bar->remove_menu('updates');          			// Remove the updates link
		$wp_admin_bar->remove_menu('comments');         			// Remove the comments link
		$wp_admin_bar->remove_menu('wpseo-menu');					// Remove the SEO link
		$wp_admin_bar->remove_menu('vc_inline-admin-bar-link');		// Remove the Visual Composer link
		$wp_admin_bar->remove_menu('itsec_admin_bar_menu');			// Remove the iThemes security link

	}

	public function remove_core_updates(){
		global $wp_version;return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);	}

	public function edit_dashboard_widgets(){
		global $wp_meta_boxes;		$wp_meta_boxes['dashboard']['normal']['core'] = array();   	$wp_meta_boxes['dashboard']['side']['core'] = array();		wp_add_dashboard_widget('dashboard_widget', 'Bienvenue dans le tableau de bord de votre site !', array( $this, 'najomie_dashboard_widget' ) );

	}

	public function najomie_dashboard_widget( $post, $callback_args ){
		?>

		<h2>Voici quelques trucs et astuces à savoir :</h2>		<p>
			<strong>Indexation de votre site</strong>
			<br />----------------------------
			<br />Nous avons soumis votre site aux principaux moteurs de recherche. Normalement dans un délais de +/- 10 jours votre site devrait être indexé dans les répertoires de ces engins de recherche.
		</p>

		<p>
			<strong>Répandez la bonne nouvelle</strong>
			<br />----------------------------
			<br />Plus vous aller parler de votre site, mieux il se portera. Il est important de créer des liens avec d'autres sites pour augementer votre crédibilité avec les engins de recherche. Publiez votre adresse sur vos outils de communications et dans vos sites de médias sociaux également tel que Facebook.
		</p>

		<p>
			<strong>Statistiques</strong>
			<br />----------------------------
			<br />Vous recevrez les statistiques de votre site internet par courriel à chaque 1er du mois. (<a href="mailto:info@najomie.com">Contactez-nous</a> si vous ne recevez pas votre rapport de statistique mensuel) Si vous désirez en savoir plus, vous pouvez nous envoyer une adresse GMAIL et nous allons vous donner accès à votre compte.
		</p>

		<p>
			<strong>Mises à jour</strong>
			<br />----------------------------
			<br />La clef du succès de votre site web est sans aucun doute la mise à jour de votre contenu. Les engins de recherche adorent le contenu frais et nouveau. Prenez le temps de nous envoyer vos modifications de textes, photos, promotions, etc. Pour vos demandes de modifications et de mises à jour, utilisez notre système de support en envoyant vos demandes à l'adressse : <a href="mailto:info@najomie.com">info@najomie.com</a>
		</p>

		<p>
			<strong>Assurance qualité</strong>
			<br />----------------------------
			<br />Nous avons mis en place un système d'assurance qualité pour votre site web. Voici une énumération des vérifications effectuées par notre équipe lors de la mise en ligne:

		</p>

		<ul style="list-style-type: disc; padding-left: 15px;">
			<li>Nous avons mise à jour Wordpress et ses extensions au moment de cet envoi</li>
			<li>Nous avons mise à jour et configuré adéquatement les extensions de sécurités au moment de cet envoi</li>
			<li>Nous avons configuré un système de "cache" pour accélérer votre site</li>
			<li>Nous avons créé un utilisateur et un mot de passe temporaire pour vous</li>
			<li>Nous avons simplifié l'interface du panneau d'administration pour vous</li>
			<li>Nous avons configuré un système de backup et de versionnage de votre site.</li>
			<li>Nous avons créé une page introuvable 404</li>
			<li>Nous avons ajouté une icone de favoris (favicon)</li>
			<li>Nous avons créé des redirections si vous aviez un ancien site web</li>
			<li>Nous avons configuré l'envois des statistiques de votre site à votre courriel (Google Analytics)</li>
			<li>Nous avons relié votre site à Google Search Console pour des statistiques plus poussées si nécessaire</li>
			<li>Nous avons ajouté votre site à notre système de "monitoring" et ainsi être averti si votre site n'est plus en ligne</li>
			<li>Nous avons testé chaque page avec un mot clefs principal pour s'assurer d'un bon référencement (SEO Yoast)</li>
			<li>Nous avons testé chaque page pour être sur que les contenus et les liens soient adéquats</li>
			<li>Nous avons testé les différents formulaires présents sur votre site web</li>
			<li>Nous avons testé l'optimisation du site web pour une meilleure vitesse de chargement</li>
			<li>Nous avons testé le site dans les navigateurs les plus populaires (IE, Firefox, Chrome, Safari)</li>
			<li>Nous avons testé le site sur tablettes et mobiles les plus populaires (iOS et Android)</li>
		</ul>

		----------------------------
		<br /><br />		<!-- <img src="http://pardesign.net/login-logo.png" alt="PAR Design" /> -->

		<?php
	}

	public function screen_layout_columns($columns){
		$columns['dashboard'] = 1;
		return $columns;
	}

	public function screen_layout_dashboard() { return 1; }

	public function remove_dashboard_help_tab( $old_help, $screen_id, $screen ){
	    if( 'dashboard' != $screen->base )
	        return $old_help;

	    $screen->remove_help_tabs();
	    return $old_help;
	}

	public function remove_help_tab( $visible ){
	    global $current_screen;
	    if( 'dashboard' == $current_screen->base )
	        return false;
	    return $visible;
	}

	/**
	 * Change login logo and logo link and maybe design aswell
	 */
	public function customize_login(){
		add_action( 'login_head', array($this, 'login_logo') );
		add_filter( 'login_headerurl', array($this, 'login_url') );
	}

	public function login_logo(){ ?>
		<style>
		    body.login>#login a:hover { color: #a6ce3a; }
			body.login>#login p.message {  border-left: 4px solid #a6ce3a; }
			body.login { background-image: url('<?php echo $this->imgURL('naj-bg.jpg'); ?>'); background-repeat: no-repeat; background-size: cover;  }
	    	.login h1 a {					background-image: url('<?php echo $this->imgURL('logo.png'); ?>'); background-color: #a6ce3a; background-position: center;					width: 150px; height: 150px; border-radius: 50%; background-size: 70%; margin-bottom: 20px; transition: 0.3s all ease;				}				.login h1 a:hover { background-color: #A0C53A; }

			@media (max-width: 968px) { body.login .left-text, body.login .right-text { visibility: hidden;} }
			#login form p.submit .button-primary {
				width: 100%; background-color: #a6ce3a; border-color: #a6ce3a;
				color: #fff; font-family: 'Open sans', sans-serif; text-transform: uppercase;
				font-size: 13px; letter-spacing: 0em; transition: all 0.3s ease; box-shadow: none;
				padding:5px 0 5px 0; text-shadow: none;
				box-sizing: content-box;
			}
			#login { padding-top: 130px; }
			#loginform { padding: 26px 24px;  }
			#login form p.submit .button-primary:hover { background-color: #969696; color: #fff;  border-color: #969696; }
			#login form p.submit .button-primary:focus { outline: none;  }
			#login .forgetmenot { margin: 0 0 15px 0; }
			#login #nav, #login #backtoblog { margin: 5px 0 0 0; padding: 0 5px; display: inline-block; }
			#login #nav a, #login #backtoblog a { font-size: 11px; color: #fff; transition: 0.3s all ease; }			#login #nav a:hover, #login #backtoblog a:hover { color: #a6ce3a; }
			#login #nav { float: left; }			#login #backtoblog {  float: right; }
		</style>
	<?php }

	public function login_url(){
		return 'http://najomie.com';
	}

	public function imgURL( $file ){
		return $this->plugin_url() . "/assets/images/{$file}";
	}

	public function jsURL( $file ){
		return $this->plugin_url() . "/assets/js/{$file}";
	}
	public function jsPATH( $file ){
		return $this->plugin_path() . "/assets/js/{$file}";
	}

	public function cssURL( $file ){
		return $this->plugin_url() . "/assets/css/{$file}";
	}
	public function cssPATH( $file ){
		return $this->plugin_path() . "/assets/css/{$file}";
	}

	public function fontsPATH( $file ){
		return $this->plugin_url() . "/assets/fonts/{$file}";
	}

	/**
	 * Get the plugin url.
	 *
	 * @access public
	 * @return string
	 */
	public function plugin_url() {
		if ( $this->plugin_url ) return $this->plugin_url;
		return $this->plugin_url = untrailingslashit( plugins_url( '/', __FILE__ ) );
	}

	/**
	 * Get the plugin path.
	 *
	 * @access public
	 * @return string
	 */
	public function plugin_path() {
		if ( $this->plugin_path ) return $this->plugin_path;
		return $this->plugin_path = untrailingslashit( plugin_dir_path( __FILE__ ) );
	}
}

// Init Class and register in global scope
$GLOBALS['NAJ_Client_Backend'] = new NAJ_Client_Backend();

} // class_exists check