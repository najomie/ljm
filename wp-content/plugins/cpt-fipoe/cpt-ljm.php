<?php
/**
 * Plugin Name: CPT LJM
 * Plugin URI: http://pardesign.net
 * Description: Custom post types pour LJM
 * Version: 1.0.0
 * Author: Gabriel Gaudreau
 * Author URI: http://najomie.com
 * Requires at least: 4.0
 * Tested up to: 4.3
 *
 * @package CPT_LJM
 * @category Core
 * @author PARDesign
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'CPT_LJM' ) ) {

class CPT_LJM
{
	const DEBUG = FALSE;

	/**
	 * Plugin version, match with plugin header
	 * @var string
	 */
	public $version = '1.0.0';

	/**
	 * Use the function not the variable
	 * @var string
	 */
	public $plugin_url;

	/**
	 * Use the function not the variable
	 * @var string
	 */
	public $plugin_path;

	/**
	 * Do we update the rewrite rules for a custom post type?
	 * @var boolean
	 */
	public $flush_rules = FALSE;

	/**
	 * PLUGIN STARTUP
	 */
	public function __construct(){
		// do something when we activate/deactivate the plugin
		register_activation_hook( __FILE__, array( $this, 'activate' ) );
		register_deactivation_hook( __FILE__, array( $this, 'deactivate' ) );

		$installed = get_option( 'CPT_FIPOE_Version', FALSE );

		if( ! $installed or version_compare($installed, $this->version, '!=') ){
			$this->flush_rules = TRUE;
			update_option( 'CPT_FIPOE_Version', $this->version );
		}

		$this->hooks();
	}

	/**
	 * Register the plugin's hooks
	 */
	public function hooks(){
		add_action( 'init', array($this, 'init'), 0 );
	}

	/**
	 * Runs on WordPress init hook
	 */
	public function init(){
		$this->post_types();
		$this->flush();
	}

	/**
	 * Register the post types and taxonomies
	 *
	 * @link https://developer.wordpress.org/resource/dashicons/ for admin menu icons
	 */
	 public function post_types(){

		 $this->tax_type();		    // CPT: Media
		 $this->cpt_recette();
	}

	public function cpt_recette(){
		$labels = array(
			'name' => _x( 'Recettes', 'recette' ),
			'singular_name' => _x( 'Recette', 'recette' ),
			'add_new' => _x( 'Ajouter une recette', 'recette' ),
			'add_new_item' => _x( 'Ajouter une nouvelle recette', 'recette' ),
			'edit_item' => _x( 'Modifier la recette', 'recette' ),
			'new_item' => _x( 'Nouvelle recette', 'recette' ),
			'view_item' => _x( 'Afficher la recette', 'recette' ),
			'search_items' => _x( 'Rechercher dans les recettes', 'recette' ),
			'not_found' => _x( 'Aucune recette trouvée', 'recette' ),
			'not_found_in_trash' => _x( 'Aucune recette trouvée dans la corbeille', 'recette' ),
			'parent_item_colon' => _x( 'Recette parente:', 'recette' ),
			'menu_name' => _x( 'Recettes', 'recette' ),
		);

		$args = array(
			'labels' => $labels,
			'hierarchical' => false,

			'supports' => array( 'title', 'editor','thumbnail' ),
			'taxonomies' => array(),
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,

			'menu_icon' => 'dashicons-carrot',
			'show_in_nav_menus' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'has_archive' => true,
			'query_var' => true,
			'can_export' => true,
			'rewrite' => array( 'slug' => 'recettes' ),
			'capability_type' => 'post'
		);

		register_post_type( 'recette', $args );
	}

	public function tax_type(){
		$labels = array(
		    'name' => _x( 'Type de recette', 'region' ),
		    'singular_name' => _x( 'Type de recette', 'type' ),
		    'search_items' => _x( 'Chercher un type', 'type' ),
		    'popular_items' => _x( 'Les plus utilisées', 'type' ),
		    'all_items' => _x( 'Tous', 'type' ),
		    'parent_item' => _x( 'Type parent', 'type' ),
		    'parent_item_colon' => _x( 'Type parent:', 'type' ),
		    'edit_item' => _x( 'Modifier le type', 'type' ),
		    'update_item' => _x( 'Mettre à jour', 'type' ),
		    'add_new_item' => _x( 'Ajouter un nouveau type de recette', 'type' ),
		    'new_item_name' => _x( 'Nouveau type de recette', 'type' ),
		    'separate_items_with_commas' => _x( 'Séparer tout les types par une virgule', 'type' ),
		    'add_or_remove_items' => _x( 'Ajouter ou supprimer des types de recette', 'type' ),
		    'choose_from_most_used' => _x( 'Choisir parmi les types les plus utilisés', 'type' ),
		    'menu_name' => _x( 'Type de recette', 'type' ),
		);

		$args = array(
			'labels' => $labels,
			'public' => TRUE,
			'show_in_nav_menus' => TRUE,
			'show_ui' => TRUE,
			'show_tagcloud' => TRUE,
			'show_admin_column' => FALSE,
			'hierarchical' => TRUE,
			'rewrite' => array('slug' => 'recettes/type'),
			'query_var' => TRUE
		);

		register_taxonomy( 'type', array('recette'), $args );
	}

	/**
	 * Refresh rewrite rules
	 */
	public function flush(){
		if( $this->flush_rules )
			flush_rewrite_rules();
	}

	public function activate(){
		$this->flush_rules = TRUE; // we will need to refresh the rewrite rules for the custom post types
	}

	public function deactivate(){
		flush_rewrite_rules();
	}

	public function imgURL( $file ){
		return $this->plugin_url() . "/assets/images/{$file}";
	}

	public function jsURL( $file ){
		return $this->plugin_url() . "/assets/js/{$file}";
	}
	public function jsPATH( $file ){
		return $this->plugin_path() . "/assets/js/{$file}";
	}

	public function cssURL( $file ){
		return $this->plugin_url() . "/assets/css/{$file}";
	}
	public function cssPATH( $file ){
		return $this->plugin_path() . "/assets/css/{$file}";
	}

	/**
	 * Get the plugin url.
	 *
	 * @access public
	 * @return string
	 */
	public function plugin_url() {
		if ( $this->plugin_url ) return $this->plugin_url;
		return $this->plugin_url = untrailingslashit( plugins_url( '/', __FILE__ ) );
	}

	/**
	 * Get the plugin path.
	 *
	 * @access public
	 * @return string
	 */
	public function plugin_path() {
		if ( $this->plugin_path ) return $this->plugin_path;
		return $this->plugin_path = untrailingslashit( plugin_dir_path( __FILE__ ) );
	}
}

// Init Class and register in global scope
$GLOBALS['CPT_LJM'] = new CPT_LJM();

} // class_exists check
