<?php if ( get_post_type() === 'recette' ) : ?>
      <article class="col-sm-4">

      	<?php if ( has_post_thumbnail() ) : ?>
      		<figure class="post-thumbnail">
      			<a href="<?php echo the_permalink(); ?>"><?php the_post_thumbnail('post-list-thumbnail'); ?>
                <h2><?php the_title(); ?></h2>
            </a>
      		</figure>
      	<?php endif; ?>

      </article>
   <?php else: ?>
      <article class="col-sm-6 list-post">
         <?php if ( has_post_thumbnail() ) : ?>
            <figure class="post-thumbnail">
               <a href="<?php echo the_permalink(); ?>"><?php the_post_thumbnail('post-list-thumbnail'); ?></a>
            </figure>
         <?php endif; ?>

         <div class="inner-meta">
            <h3><?php the_title(); ?></h3>
            <?php naj_excerpt(240);?>
            <a class="more" href="<?php echo get_permalink(); ?>">Lire la suite  ?</a>
         </div>
      </article>
   <?php endif; ?>